<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePrestadoresServicios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_prestador_servicios', function (Blueprint $table) {
            $table->string('codigo')->nullable(); 
            $table->string('direccion')->nullable(); 
            $table->string('telefono')->nullable(); 
            $table->string('nro_plano')->nullable(); 
            $table->integer('id_municipio')->nullable(); 
            $table->string('representante')->nullable(); 
            $table->string('email')->nullable(); 
            $table->string('prestamo')->nullable(); 
            $table->integer('id_homocontable')->nullable(); 
            $table->string('tipo_documento')->nullable(); 
            $table->string('numero_documento')->nullable(); 
            $table->string('nombre_consolidado')->nullable(); 
            $table->string('nombre_detallado')->nullable(); 
            $table->string('nombre_capitado')->nullable(); 
            $table->string('tipide_sidem')->nullable(); 
            $table->string('dig_verificacion')->nullable(); 
            $table->string('maneja_particulares')->nullable(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_prestador_servicios', function (Blueprint $table) {
            //
        });
    }
}
