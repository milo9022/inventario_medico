<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCupsXPrestadorServicioAnno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_cups_x_prestador_servicio_anno', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_prestador_servicio');
            $table->integer('anno');
            $table->double('porcentaje');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_cups_x_prestador_servicio_anno');
    }
}
