<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblCupsXPrestadorServicio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_cups_x_prestador_servicios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cups');
            $table->double('tarifa_valor');
            $table->integer('id_cups_x_prestador_servicios_x_anno');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_cups_x_prestador_servicios');
    }
}
