<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblCupsCantidad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_cups_cantidad', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cups');
            $table->integer('id_prestador_servicios');
            $table->date('fecha');
            $table->integer('cantidad_cups');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbl_cups_cantidad', function (Blueprint $table) {
            //
        });
    }
}
