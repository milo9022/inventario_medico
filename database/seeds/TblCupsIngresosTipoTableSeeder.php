<?php

use Illuminate\Database\Seeder;

class TblCupsIngresosTipoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_cups_ingresos_tipo')->delete();
        
        \DB::table('tbl_cups_ingresos_tipo')->insert(array (
            0 => 
            array (
                'id' => 1,
                'descripcion' => 'Ingresos operacionales',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'descripcion' => 'Ingresos no operacionales',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'descripcion' => 'Extraordinarios',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'descripcion' => 'Margen de contratacion',
                'deleted_at' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}