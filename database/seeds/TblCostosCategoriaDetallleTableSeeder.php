<?php

use Illuminate\Database\Seeder;

class TblCostosCategoriaDetallleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_costos_categoria_detallle')->delete();
        
        \DB::table('tbl_costos_categoria_detallle')->insert(array (
            0 => 
            array (
                'id' => 1,
                'codigo' => 'R1',
                'descripcion' => 'PLANTA',
                'id_costos_categoria' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'codigo' => 'R2',
                'descripcion' => 'CONTRATO DIRECTO',
                'id_costos_categoria' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'codigo' => 'R3',
                'descripcion' => 'CONTRATO CON TERCEROS',
                'id_costos_categoria' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'codigo' => 'R4',
                'descripcion' => 'MEDICAMENTOS',
                'id_costos_categoria' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'codigo' => 'R5',
                'descripcion' => 'DISPOSITIVOS MÉDICOS',
                'id_costos_categoria' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'codigo' => 'R6',
                'descripcion' => 'REACTIVOS DE DIAGNÓSTICO',
                'id_costos_categoria' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'codigo' => 'R7',
                'descripcion' => 'OTROS ELEMENTOS DE CONSUMO MÉDICO ASISTENCIALES',
                'id_costos_categoria' => 2,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'codigo' => 'R8',
                'descripcion' => 'ARRENDAMIENTO',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'codigo' => 'R9',
                'descripcion' => 'VIÁTICOS Y GASTOS DE VIAJE',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'codigo' => 'R10',
                'descripcion' => 'COMUNICACIÓN  Y TRANSPORTE',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'codigo' => 'R11',
                'descripcion' => 'COMBUSTIBLES  Y LUBRICANTES',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'codigo' => 'R12',
                'descripcion' => 'ELEMENTOS DE CONSUMO DE OFICINA',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'codigo' => 'R13',
                'descripcion' => 'SEGUROS',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'codigo' => 'R14',
                'descripcion' => 'ENERGÍA',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'codigo' => 'R15',
                'descripcion' => 'ACUEDUCTO, ALCANTARILLADO  Y ASEO PÚBLICO',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'codigo' => 'R16',
                'descripcion' => 'GAS',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'codigo' => 'R17',
                'descripcion' => 'SENTENCIAS JUDICIALES',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'codigo' => 'R18',
                'descripcion' => 'IMPUESTOS, CONTRIBUCIONES, TASAS Y MULTAS',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'codigo' => 'R19',
                'descripcion' => 'VIGILANCIA Y SEGURIDAD',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'codigo' => 'R20',
                'descripcion' => 'MANTENIMIENTO Y REPARACIONES',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'codigo' => 'R21',
                'descripcion' => 'ASEO, CAFETERÍA Y LIMPIEZA',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'codigo' => 'R22',
                'descripcion' => 'LAVANDERÍA',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'codigo' => 'R23',
                'descripcion' => 'ALIMENTACIÓN',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'codigo' => 'R24',
                'descripcion' => 'OTROS GASTOS GENERALES',
                'id_costos_categoria' => 3,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'codigo' => 'R25',
                'descripcion' => 'PROVISIONES',
                'id_costos_categoria' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'codigo' => 'R26',
                'descripcion' => 'DEPRECIACIONES',
                'id_costos_categoria' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'codigo' => 'R27',
                'descripcion' => 'AMORTIZACIONES',
                'id_costos_categoria' => 4,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}