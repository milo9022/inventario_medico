<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_documento ='user';
        $admin_documento='admin';
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        
        $user = new User();
        $user->name = 'User';
        $user->email = 'user@user.com';
        $user->documento = $user_documento;
        $user->password = bcrypt($user_documento);
        $user->save();
        $user->roles()->attach($role_user);

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@admin.com';
        $user->documento = $admin_documento;
        $user->password = bcrypt($admin_documento);
        $user->save();
        $user->roles()->attach($role_admin);
    }
}