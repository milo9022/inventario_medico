<?php

use Illuminate\Database\Seeder;

class TblCentroCostosTipoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_centro_costos_tipo')->delete();
        
        \DB::table('tbl_centro_costos_tipo')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'APOYO DIAGNÓSTICO',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'APOYO MÉDICO',
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'APOYO TERAPÉUTICO',
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'CIRUGÍA',
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'CONSULTA EXTERNAY PROCEDIMIENTOS SUBESPECIALIDADES',
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'HOSPITALIZACIÓN',
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'PLAN DE INTERVENCIONES COLECTIVAS',
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'PROCEDIMIENTOS DE ATENCION GINECOOBSTETRICA',
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'UNIDADES ESPECIALES',
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'URGENCIAS',
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'ODONTOLOGÍA',
            ),
        ));
        
        
    }
}