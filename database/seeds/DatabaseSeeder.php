<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // La creación de datos de roles debe ejecutarse primero
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(TblPrestadorServiciosTableSeeder::class);
        $this->call(TblCostosCategoriaTableSeeder::class);
        $this->call(TblCostosCategoriaDetallleTableSeeder::class);
        $this->call(TblCupsTableSeeder::class);
        $this->call(TblCupsIngresosTipoTableSeeder::class);
        $this->call(TblCentroCostosTipoTableSeeder::class);
        $this->call(TblCentroCostosTableSeeder::class);
        $this->call(TblCupsTarifaTableSeeder::class);
    }
}
