<?php

use Illuminate\Database\Seeder;

class TblCentroCostosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_centro_costos')->delete();
        
        \DB::table('tbl_centro_costos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'AUDIOLOGIA Y AUDIOMETRÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'CARDIOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre' => 'DERMATOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre' => 'ENDOCRINOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre' => 'GERIATRÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre' => 'GASTROENTEROLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A06',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre' => 'GENÉTICA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A07',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre' => 'INMUNOHEMATOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A08',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre' => 'NEFROLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A09',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nombre' => 'MEDICINA GENERAL',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A10',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nombre' => 'MEDICINA INTERNA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A11',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nombre' => 'NEUMOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A12',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'nombre' => 'NEUROLOGÍA Y NEUROPEDIATRÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A13',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'nombre' => 'ONCOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A14',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'nombre' => 'OPTOMETRÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A15',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'nombre' => 'OTORRINOLARINGOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A16',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'nombre' => 'PSIQUIATRÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A17',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'nombre' => 'PSICOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A18',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'nombre' => 'REUMATOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A19',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'nombre' => 'UROLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A20',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'nombre' => 'OFTALMOLOGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A21',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'nombre' => 'MAXILOFACIAL',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A22',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'nombre' => 'CIRUGIA GENERAL',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A23',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'nombre' => 'GINECOLOGÍA Y OBSTETRICIA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A24',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'nombre' => 'ORTOPEDIA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A25',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'nombre' => 'PEDIATRÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A26',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'nombre' => 'PLÁSTICA,  ESTÉTICA Y RECONSTRUCTIVA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A27',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'nombre' => 'CONSULTA PREANESTÉSICA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A28',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'nombre' => 'NEUROCIRUGÍA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A29',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'nombre' => 'TORAX',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A30',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'nombre' => 'CARDIOVASCULAR',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A31',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'nombre' => 'OTRAS CONSULTAS Y PROCEDIMIENTOS',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A32',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'nombre' => 'VASCULAR PERIFERICA',
                'id_centro_costos_tipo' => 5,
                'codigo_ceco' => 'A33',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'nombre' => 'CONSULTA URGENCIAS',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'B01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'nombre' => 'OBSERVACIÓN ',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'B02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'nombre' => 'PROCEDIMIENTOS  URGENCIAS',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'B03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'nombre' => 'URGENCIAS ESPECIALIZADAS',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'B04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'nombre' => 'URGENCIAS PEDIÁTRICAS',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'B05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'nombre' => 'CIRUGÍA GENERAL',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'nombre' => 'MAXILOFACIAL',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'nombre' => 'PLÁSTICA, ESTÉTICA Y RECONSTRUCTIVA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'nombre' => 'NEUROCIRUGÍA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'nombre' => 'CARDIOVASCULAR',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'nombre' => 'DERMATOLOGÍA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C06',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'nombre' => 'ENDOCRINOLOGÍA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C07',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'nombre' => 'GASTROINTESTINAL',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C08',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'nombre' => 'UROLOGÍA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C09',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'nombre' => 'GINECOLOGÍA Y OBSTETRICIA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C10',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'nombre' => 'ORTOPEDIA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C11',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'nombre' => 'ONCOLOGÍA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C12',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'nombre' => 'PEDIATRÍA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C13',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'nombre' => 'OFTALMOLOGÍA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C14',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'nombre' => 'CABEZA Y CUELLO',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C15',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'nombre' => 'OTORRINOLARINGOLOGÍA',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C16',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'nombre' => 'TORAX',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C17',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'nombre' => 'VASCULAR PERIFERICO',
                'id_centro_costos_tipo' => 4,
                'codigo_ceco' => 'C18',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'nombre' => 'ATENCIÓN DE PARTO BAJA COMPLEJIDAD',
                'id_centro_costos_tipo' => 8,
                'codigo_ceco' => 'D02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'nombre' => 'PROCEDIMIENTOS GINECOBSTETRICIA',
                'id_centro_costos_tipo' => 8,
                'codigo_ceco' => 'D03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'nombre' => 'ATENCIÓN DE PARTO ALTA COMPLEJIDAD',
                'id_centro_costos_tipo' => 8,
                'codigo_ceco' => 'D05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'nombre' => 'ODONTOLOGÍA GENERAL',
                'id_centro_costos_tipo' => 11,
                'codigo_ceco' => 'E01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'nombre' => 'ODONTOLOGÍA PEDIÁTRICA',
                'id_centro_costos_tipo' => 11,
                'codigo_ceco' => 'E02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'nombre' => 'ORTODONCIA',
                'id_centro_costos_tipo' => 11,
                'codigo_ceco' => 'E03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'nombre' => 'REHABILITACIÓN ORAL',
                'id_centro_costos_tipo' => 11,
                'codigo_ceco' => 'E04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
            'nombre' => 'CIRUGÍA ORAL (AMBULATORIA)',
                'id_centro_costos_tipo' => 11,
                'codigo_ceco' => 'E05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'nombre' => 'HOSPITALIZACIÓN GENERAL',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'nombre' => 'ORTOPEDIA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'nombre' => 'GINECOLOGÍA Y OBSTETRICIA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'nombre' => 'OFTALMOLOGÍA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'nombre' => 'PEDIATRÍA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'nombre' => 'MEDICINA INTERNA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I06',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'nombre' => 'MAXILOFACIAL',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I07',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'nombre' => 'PLÁSTICA,  ESTÉTICA Y RECONSTRUCTIVA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I08',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'nombre' => 'NEUROCIRUGÍA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I09',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'nombre' => 'CARDIOVASCULAR',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I10',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'nombre' => 'ENDOCRINOLÓGICA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I11',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'nombre' => 'GASTROINTESTINAL',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I12',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'nombre' => 'ONCOLOGÍA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I13',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'nombre' => 'UROLOGIA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I14',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'nombre' => 'PERINATOLOGÍA Y NEONATOLOGÍA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I15',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'nombre' => 'NEUMOLOGIA',
                'id_centro_costos_tipo' => 6,
                'codigo_ceco' => 'I16',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'nombre' => 'ANCIANATOS Y ALBERGUES',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'nombre' => 'UCI ADULTOS',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'nombre' => 'UCI PEDIÁTRICA',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'nombre' => 'UCI NEONATAL',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'nombre' => 'UCI INTERMEDIOS ADULTOS',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'nombre' => 'UCI INTERMEDIOS PEDIATRICA',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J06',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'nombre' => 'UCI INTERMEDIO NEONATAL',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J07',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'nombre' => 'UNIDAD DE QUEMADOS ADULTOS',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J08',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'nombre' => 'UNIDAD DE RECIÉN NACIDOS',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J09',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
            'nombre' => 'UNIDAD SALUD MENTAL Y DESINTOXICACIÓN (BAJA COMPLEJIDAD)',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J10',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'nombre' => 'INTERNACIÓN PARCIAL',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J11',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'nombre' => 'UNIDAD DE AISLAMIENTO',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J12',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'nombre' => 'OTRAS UNIDADES ESPECIALES',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J13',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'nombre' => 'UNIDAD DE QUEMADOS PEDIATRÍA',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J14',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
            'nombre' => 'UNIDAD SALUD MENTALY DESINTOXICACIÓN (ALTA COMPLEJIDAD)',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J15',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'nombre' => 'PLAN CANGURO',
                'id_centro_costos_tipo' => 9,
                'codigo_ceco' => 'J16',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'nombre' => 'GESTIÓN DE LA VIGILANCIA EN SALUD',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K07',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'nombre' => 'PROYECTOS LOCALIDADES',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K08',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'nombre' => 'INSPECCIÓN  VIGILANCIA Y CONTROL',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K09',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'nombre' => 'GESTIÓN Y ADMINISTRACIÓN DEL PROGRAMA AMPLIADO DE INMUNIZACIONES',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K10',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'nombre' => 'SALUD INFANTIL EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K11',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'nombre' => 'SALUD SEXUAL Y REPRODUCTIVA EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K12',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'nombre' => 'PREVENCIÓN ENFERMEDADES TRANSMISIBLES Y ZOONOSIS EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K13',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'nombre' => 'PREVENCIÓN ENFERMEDADES CRÓNICAS NO TRANSMISIBLES EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K14',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'nombre' => 'SALUD ORAL EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K15',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'nombre' => 'SEGURIDAD ALIMENTARIA Y NUTRICIONAL EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K16',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'nombre' => 'ENTORNOS SALUDABLES EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K17',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'nombre' => 'SEGURIDAD EN EL TRABAJO Y PREVENCIÓN ENFERMEDADES ORIGEN LABORAL EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K18',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'nombre' => 'GESTIÓN DEL PLAN TERRITORIAL EN SALUD EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K19',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'nombre' => 'SALUD Y PREVENCIÓN ENFERMEDAD GRUPOS POBLACIONES ESPECIALES EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K20',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'nombre' => 'IMPLEMENTACIÓN POLÍTICA DISTRITAL DE PREVENCIÓN DE LA DISCAPACIDAD EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K21',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'nombre' => 'PREVALENCIA ACTIVIDAD FÍSICA EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K22',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'nombre' => 'SALUD MENTAL Y PREVENCIÓN LESIONES VIOLENTAS EVITABLES EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K23',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'nombre' => 'INTERVENCIÓN DE PERSONAS EN SITUACIÓN DESPLAZAMIENTO EN LOS TERRITORIOS',
                'id_centro_costos_tipo' => 7,
                'codigo_ceco' => 'K24',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'nombre' => 'LABORATORIO ANATOMÍA PATOLÓGICA',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'L01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'nombre' => 'IMAGENEOLOGÍA RADIOLÓGICA',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'L02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'nombre' => 'IMAGENOLOGÍA RADIOLÓGICA ESPECIAL',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'L03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'nombre' => 'IMAGENOLOGÍA NO RADIOLÓGICA',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'L04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'nombre' => 'MEDICINA NUCLEAR ',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'L05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'nombre' => 'RESONANCIA MAGNÉTICA Y T.A.C.',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'L06',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'nombre' => 'LABORATORIO Y ANÁLISIS CLÍNICO',
                'id_centro_costos_tipo' => 1,
                'codigo_ceco' => 'L07',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'nombre' => 'BANCO DE SANGRE',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'nombre' => 'HEMOCENTRO BANCO DE ÓRGANOS Y TEJIDOS COMPONENTES ANATÓMICOS',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'nombre' => 'TERAPIA RENAL',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'nombre' => 'OXÍGENO',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'nombre' => 'FARMACIA',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'nombre' => 'FISIOTERAPIA Y REHABILITACIÓN',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M06',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'nombre' => 'OTRAS TERAPIAS',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M07',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'nombre' => 'MEDICINA NUCLEAR TERAPÉUTICA',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M08',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
            'nombre' => 'SALAS ENFERMEDAD RESPIRATORIA AGUDA (ERA)',
                'id_centro_costos_tipo' => 3,
                'codigo_ceco' => 'M09',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'nombre' => 'ATENCIÓN MÉDICA DOMICILIARIA',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N01',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'nombre' => 'NUTRICION Y DIETÉTICA',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N02',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'nombre' => 'PROMOCIÓN Y PREVENCIÓN',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N03',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'nombre' => 'OTROS ENFERMERÍA',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N04',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'nombre' => 'ESTERILIZACIÓN',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N05',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'nombre' => 'INFECTOLOGÍA',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N06',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'nombre' => 'LACTARIO',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N07',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'nombre' => 'SERVICIO DE AMBULANCIAS',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N08',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'nombre' => 'TOXICOLOGÍA',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N09',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'nombre' => 'VIGILANCIA EPIDEMIOLÓGICA Y MEDIO AMBIENTE',
                'id_centro_costos_tipo' => 2,
                'codigo_ceco' => 'N10',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}