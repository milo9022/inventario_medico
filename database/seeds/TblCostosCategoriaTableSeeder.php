<?php

use Illuminate\Database\Seeder;

class TblCostosCategoriaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_costos_categoria')->delete();
        
        \DB::table('tbl_costos_categoria')->insert(array (
            0 => 
            array (
                'id' => 1,
                'descripcion' => 'COSTOS DE PERSONAL',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'descripcion' => 'INSUMOS HOSPITALARIOS',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'descripcion' => 'COSTOS GENERALES',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'descripcion' => 'ESTIMACIONES CONTABLES',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}