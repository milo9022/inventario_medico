<?php

use Illuminate\Database\Seeder;

class TblCupsTarifaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tbl_cups_tarifa')->delete();
        
        \DB::table('tbl_cups_tarifa')->insert(array (
            0 => 
            array (
                'id' => 1,
                'id_cup' => 1907,
                'valor_tarifa' => '3185585.1',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'id_cup' => 2023,
                'valor_tarifa' => '3231301.28',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'id_cup' => 2024,
                'valor_tarifa' => '894771.76',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'id_cup' => 712,
                'valor_tarifa' => '45039.06',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'id_cup' => 1935,
                'valor_tarifa' => '56074',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'id_cup' => 2282,
                'valor_tarifa' => '50974.44',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'id_cup' => 2325,
                'valor_tarifa' => '277978.92',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'id_cup' => 2310,
                'valor_tarifa' => '1199900',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'id_cup' => 2311,
                'valor_tarifa' => '236748.66',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'id_cup' => 2347,
                'valor_tarifa' => '1318900',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'id_cup' => 2348,
                'valor_tarifa' => '316066.92',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'id_cup' => 2284,
                'valor_tarifa' => '4821845.58',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'id_cup' => 2285,
                'valor_tarifa' => '3170233.52',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'id_cup' => 2286,
                'valor_tarifa' => '7422240.3',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'id_cup' => 2315,
                'valor_tarifa' => '7058436.42',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'id_cup' => 2316,
                'valor_tarifa' => '3301880.46',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'id_cup' => 2317,
                'valor_tarifa' => '4102426.74',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'id_cup' => 2354,
                'valor_tarifa' => '174559.42',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'id_cup' => 2355,
                'valor_tarifa' => '276518.88',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'id_cup' => 2096,
                'valor_tarifa' => '62411.42',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'id_cup' => 2400,
                'valor_tarifa' => '82576.9',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'id_cup' => 2112,
                'valor_tarifa' => '139941.66',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'id_cup' => 2119,
                'valor_tarifa' => '174559.42',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'id_cup' => 2165,
                'valor_tarifa' => '295679.26',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'id_cup' => 939,
                'valor_tarifa' => '15446.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'id_cup' => 946,
                'valor_tarifa' => '15457.38',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'id_cup' => 904,
                'valor_tarifa' => '77763',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'id_cup' => 1001,
                'valor_tarifa' => '240779.64',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'id_cup' => 2243,
                'valor_tarifa' => '35443',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'id_cup' => 2251,
                'valor_tarifa' => '2762173.5',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'id_cup' => 3732,
                'valor_tarifa' => '463076.02',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'id_cup' => 2252,
                'valor_tarifa' => '391809.14',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'id_cup' => 387,
                'valor_tarifa' => '106329',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'id_cup' => 391,
                'valor_tarifa' => '43166.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'id_cup' => 417,
                'valor_tarifa' => '8569.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'id_cup' => 501,
                'valor_tarifa' => '74694.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'id_cup' => 43,
                'valor_tarifa' => '465837.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'id_cup' => 523,
                'valor_tarifa' => '38299.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'id_cup' => 523,
                'valor_tarifa' => '46446.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'id_cup' => 524,
                'valor_tarifa' => '39886.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'id_cup' => 527,
                'valor_tarifa' => '154468',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'id_cup' => 561,
                'valor_tarifa' => '78926.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'id_cup' => 573,
                'valor_tarifa' => '15446.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'id_cup' => 580,
                'valor_tarifa' => '40415.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'id_cup' => 595,
                'valor_tarifa' => '57872.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'id_cup' => 1066,
                'valor_tarifa' => '15129.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'id_cup' => 1068,
                'valor_tarifa' => '15341',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'id_cup' => 1097,
                'valor_tarifa' => '62739.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'id_cup' => 251,
                'valor_tarifa' => '13648.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'id_cup' => 252,
                'valor_tarifa' => '26238.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'id_cup' => 149,
                'valor_tarifa' => '15975.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'id_cup' => 1152,
                'valor_tarifa' => '9204.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'id_cup' => 1154,
                'valor_tarifa' => '23170.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'id_cup' => 1158,
                'valor_tarifa' => '37347.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'id_cup' => 1185,
                'valor_tarifa' => '37347.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'id_cup' => 612,
                'valor_tarifa' => '33009.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'id_cup' => 612,
                'valor_tarifa' => '37135.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'id_cup' => 1202,
                'valor_tarifa' => '41791',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'id_cup' => 1226,
                'valor_tarifa' => '23910.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'id_cup' => 1248,
                'valor_tarifa' => '23910.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'id_cup' => 1264,
                'valor_tarifa' => '22535.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'id_cup' => 1271,
                'valor_tarifa' => '77234',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'id_cup' => 1273,
                'valor_tarifa' => '45176.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'id_cup' => 192,
                'valor_tarifa' => '100827.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'id_cup' => 1290,
                'valor_tarifa' => '39040.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'id_cup' => 1294,
                'valor_tarifa' => '17880.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'id_cup' => 2258,
                'valor_tarifa' => '68875.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 68,
                'id_cup' => 2253,
                'valor_tarifa' => '63585.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 69,
                'id_cup' => 2254,
                'valor_tarifa' => '465943.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 70,
                'id_cup' => 1313,
                'valor_tarifa' => '465869.14',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 71,
                'id_cup' => 2264,
                'valor_tarifa' => '58295.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 72,
                'id_cup' => 2266,
                'valor_tarifa' => '35760.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 73,
                'id_cup' => 673,
                'valor_tarifa' => '117014.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 74,
                'id_cup' => 680,
                'valor_tarifa' => '80619.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 75,
                'id_cup' => 685,
                'valor_tarifa' => '57555.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 76,
                'id_cup' => 1499,
                'valor_tarifa' => '28671.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 77,
                'id_cup' => 1506,
                'valor_tarifa' => '58295.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 78,
                'id_cup' => 1534,
                'valor_tarifa' => '69616.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 79,
                'id_cup' => 1535,
                'valor_tarifa' => '102414.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 80,
                'id_cup' => 1536,
                'valor_tarifa' => '65278.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 81,
                'id_cup' => 1538,
                'valor_tarifa' => '96172.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 82,
                'id_cup' => 1539,
                'valor_tarifa' => '73742.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 83,
                'id_cup' => 1540,
                'valor_tarifa' => '115745.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 84,
                'id_cup' => 1541,
                'valor_tarifa' => '85486.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 85,
                'id_cup' => 1542,
                'valor_tarifa' => '74060',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 86,
                'id_cup' => 1545,
                'valor_tarifa' => '57555.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 87,
                'id_cup' => 1546,
                'valor_tarifa' => '70568.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 88,
                'id_cup' => 1549,
                'valor_tarifa' => '46763.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 89,
                'id_cup' => 1550,
                'valor_tarifa' => '46869.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 90,
                'id_cup' => 1551,
                'valor_tarifa' => '65490.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 91,
                'id_cup' => 1553,
                'valor_tarifa' => '105059.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 92,
                'id_cup' => 1554,
                'valor_tarifa' => '42637.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 93,
                'id_cup' => 1555,
                'valor_tarifa' => '105059.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 94,
                'id_cup' => 1563,
                'valor_tarifa' => '105059.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 95,
                'id_cup' => 1589,
                'valor_tarifa' => '105059.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 96,
                'id_cup' => 1590,
                'valor_tarifa' => '105059.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 97,
                'id_cup' => 1594,
                'valor_tarifa' => '105059.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 98,
                'id_cup' => 1623,
                'valor_tarifa' => '58507.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 99,
                'id_cup' => 1625,
                'valor_tarifa' => '67712',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 100,
                'id_cup' => 1696,
                'valor_tarifa' => '268308.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 101,
                'id_cup' => 1751,
                'valor_tarifa' => '33221.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 102,
                'id_cup' => 1804,
                'valor_tarifa' => '32480.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 103,
                'id_cup' => 1848,
                'valor_tarifa' => '38828.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 104,
                'id_cup' => 1855,
                'valor_tarifa' => '70462.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 105,
                'id_cup' => 2188,
                'valor_tarifa' => '141560.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 106,
                'id_cup' => 2202,
                'valor_tarifa' => '155314.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 107,
                'id_cup' => 614,
                'valor_tarifa' => '117014.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 108,
                'id_cup' => 329,
                'valor_tarifa' => '142724.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 109,
                'id_cup' => 331,
                'valor_tarifa' => '121141',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 110,
                'id_cup' => 332,
                'valor_tarifa' => '199433',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 111,
                'id_cup' => 222,
                'valor_tarifa' => '67712',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 112,
                'id_cup' => 1009,
                'valor_tarifa' => '107281.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 113,
                'id_cup' => 1010,
                'valor_tarifa' => '72473',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 114,
                'id_cup' => 1011,
                'valor_tarifa' => '111724.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 115,
                'id_cup' => 223,
                'valor_tarifa' => '10368.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 116,
                'id_cup' => 223,
                'valor_tarifa' => '47504.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 117,
                'id_cup' => 223,
                'valor_tarifa' => '111830.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 118,
                'id_cup' => 254,
                'valor_tarifa' => '111830.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 119,
                'id_cup' => 281,
                'valor_tarifa' => '59988.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 120,
                'id_cup' => 29,
                'valor_tarifa' => '39251.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 121,
                'id_cup' => 30,
                'valor_tarifa' => '64749.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 122,
                'id_cup' => 224,
                'valor_tarifa' => '50466.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 123,
                'id_cup' => 285,
                'valor_tarifa' => '22006.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 124,
                'id_cup' => 226,
                'valor_tarifa' => '42002.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 125,
                'id_cup' => 2671,
                'valor_tarifa' => '19467.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 126,
                'id_cup' => 2672,
                'valor_tarifa' => '133837',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 127,
                'id_cup' => 2674,
                'valor_tarifa' => '31105.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 128,
                'id_cup' => 2676,
                'valor_tarifa' => '69616.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 129,
                'id_cup' => 2680,
                'valor_tarifa' => '31528.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 130,
                'id_cup' => 2681,
                'valor_tarifa' => '28037',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 131,
                'id_cup' => 2682,
                'valor_tarifa' => '86015.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 132,
                'id_cup' => 2683,
                'valor_tarifa' => '31422.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 133,
                'id_cup' => 2684,
                'valor_tarifa' => '33432.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 134,
                'id_cup' => 2685,
                'valor_tarifa' => '25392',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 135,
                'id_cup' => 2686,
                'valor_tarifa' => '23910.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 136,
                'id_cup' => 2973,
                'valor_tarifa' => '36395.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 137,
                'id_cup' => 2974,
                'valor_tarifa' => '45811.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 138,
                'id_cup' => 2975,
                'valor_tarifa' => '33432.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 139,
                'id_cup' => 2976,
                'valor_tarifa' => '85486.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 140,
                'id_cup' => 2977,
                'valor_tarifa' => '61152.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 141,
                'id_cup' => 2978,
                'valor_tarifa' => '61152.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 142,
                'id_cup' => 2687,
                'valor_tarifa' => '60940.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 143,
                'id_cup' => 2688,
                'valor_tarifa' => '131932.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 144,
                'id_cup' => 2689,
                'valor_tarifa' => '10474.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 145,
                'id_cup' => 2690,
                'valor_tarifa' => '13542.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 146,
                'id_cup' => 2691,
                'valor_tarifa' => '15975.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 147,
                'id_cup' => 2692,
                'valor_tarifa' => '14706.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 148,
                'id_cup' => 2693,
                'valor_tarifa' => '51947.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 149,
                'id_cup' => 2694,
                'valor_tarifa' => '46552',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 150,
                'id_cup' => 2696,
                'valor_tarifa' => '13225',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 151,
                'id_cup' => 2697,
                'valor_tarifa' => '28142.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 152,
                'id_cup' => 2699,
                'valor_tarifa' => '33221.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 153,
                'id_cup' => 2701,
                'valor_tarifa' => '34596.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 154,
                'id_cup' => 2702,
                'valor_tarifa' => '32480.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 155,
                'id_cup' => 2703,
                'valor_tarifa' => '84005.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 156,
                'id_cup' => 2704,
                'valor_tarifa' => '93950.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 157,
                'id_cup' => 2705,
                'valor_tarifa' => '37241.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 158,
                'id_cup' => 2706,
                'valor_tarifa' => '70357',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 159,
                'id_cup' => 2708,
                'valor_tarifa' => '17033.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 160,
                'id_cup' => 2710,
                'valor_tarifa' => '10199.12',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 161,
                'id_cup' => 2711,
                'valor_tarifa' => '17033.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 162,
                'id_cup' => 2712,
                'valor_tarifa' => '73002',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 163,
                'id_cup' => 2756,
                'valor_tarifa' => '9098.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 164,
                'id_cup' => 2757,
                'valor_tarifa' => '9077.64',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 165,
                'id_cup' => 2758,
                'valor_tarifa' => '13859.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 166,
                'id_cup' => 2759,
                'valor_tarifa' => '27931.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 167,
                'id_cup' => 2714,
                'valor_tarifa' => '14388.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 168,
                'id_cup' => 2980,
                'valor_tarifa' => '36289.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 169,
                'id_cup' => 2981,
                'valor_tarifa' => '15341',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 170,
                'id_cup' => 2982,
                'valor_tarifa' => '81042.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 171,
                'id_cup' => 2715,
                'valor_tarifa' => '21160',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 172,
                'id_cup' => 2716,
                'valor_tarifa' => '51207.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 173,
                'id_cup' => 2718,
                'valor_tarifa' => '29624',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 174,
                'id_cup' => 2760,
                'valor_tarifa' => '21160',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 175,
                'id_cup' => 2762,
                'valor_tarifa' => '13225',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 176,
                'id_cup' => 2763,
                'valor_tarifa' => '84428.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 177,
                'id_cup' => 2719,
                'valor_tarifa' => '181447',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 178,
                'id_cup' => 2720,
                'valor_tarifa' => '13648.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 179,
                'id_cup' => 2721,
                'valor_tarifa' => '37135.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 180,
                'id_cup' => 2835,
                'valor_tarifa' => '36077.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 181,
                'id_cup' => 2836,
                'valor_tarifa' => '17986',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 182,
                'id_cup' => 2983,
                'valor_tarifa' => '22852.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 183,
                'id_cup' => 2837,
                'valor_tarifa' => '12167',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 184,
                'id_cup' => 2723,
                'valor_tarifa' => '66019.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 185,
                'id_cup' => 2725,
                'valor_tarifa' => '27931.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 186,
                'id_cup' => 2726,
                'valor_tarifa' => '53958',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 187,
                'id_cup' => 2727,
                'valor_tarifa' => '204723',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 188,
                'id_cup' => 2728,
                'valor_tarifa' => '37241.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 189,
                'id_cup' => 2730,
                'valor_tarifa' => '149072.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 190,
                'id_cup' => 2731,
                'valor_tarifa' => '62845.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 191,
                'id_cup' => 2732,
                'valor_tarifa' => '279629.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 192,
                'id_cup' => 2733,
                'valor_tarifa' => '55756.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 193,
                'id_cup' => 2734,
                'valor_tarifa' => '107281.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 194,
                'id_cup' => 2735,
                'valor_tarifa' => '84111',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 195,
                'id_cup' => 2738,
                'valor_tarifa' => '57026.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 196,
                'id_cup' => 2739,
                'valor_tarifa' => '16293.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 197,
                'id_cup' => 2740,
                'valor_tarifa' => '50360.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 198,
                'id_cup' => 2742,
                'valor_tarifa' => '69510.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 199,
                'id_cup' => 2743,
                'valor_tarifa' => '53746.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 200,
                'id_cup' => 2744,
                'valor_tarifa' => '71520.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 201,
                'id_cup' => 2745,
                'valor_tarifa' => '16716.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 202,
                'id_cup' => 2746,
                'valor_tarifa' => '10156.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 203,
                'id_cup' => 2747,
                'valor_tarifa' => '78926.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 204,
                'id_cup' => 2748,
                'valor_tarifa' => '25392',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 205,
                'id_cup' => 2749,
                'valor_tarifa' => '37135.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 206,
                'id_cup' => 2751,
                'valor_tarifa' => '18197.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 207,
                'id_cup' => 2752,
                'valor_tarifa' => '50360.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 208,
                'id_cup' => 2753,
                'valor_tarifa' => '310840.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 209,
                'id_cup' => 2754,
                'valor_tarifa' => '310840.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 210,
                'id_cup' => 2767,
                'valor_tarifa' => '49831.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 211,
                'id_cup' => 2768,
                'valor_tarifa' => '13225',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 212,
                'id_cup' => 2770,
                'valor_tarifa' => '47398.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 213,
                'id_cup' => 2772,
                'valor_tarifa' => '47398.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 214,
                'id_cup' => 2782,
                'valor_tarifa' => '66759.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 215,
                'id_cup' => 2785,
                'valor_tarifa' => '74800.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 216,
                'id_cup' => 2786,
                'valor_tarifa' => '39251.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 217,
                'id_cup' => 2787,
                'valor_tarifa' => '186313.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 218,
                'id_cup' => 2791,
                'valor_tarifa' => '24545.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 219,
                'id_cup' => 2793,
                'valor_tarifa' => '26026.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 220,
                'id_cup' => 2796,
                'valor_tarifa' => '26026.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 221,
                'id_cup' => 2797,
                'valor_tarifa' => '26026.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 222,
                'id_cup' => 2800,
                'valor_tarifa' => '7617.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 223,
                'id_cup' => 2801,
                'valor_tarifa' => '18726.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 224,
                'id_cup' => 2802,
                'valor_tarifa' => '31211',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 225,
                'id_cup' => 2805,
                'valor_tarifa' => '14600.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 226,
                'id_cup' => 2806,
                'valor_tarifa' => '90035.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 227,
                'id_cup' => 2807,
                'valor_tarifa' => '20842.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 228,
                'id_cup' => 3013,
                'valor_tarifa' => '8358.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 229,
                'id_cup' => 18,
                'valor_tarifa' => '19467.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 230,
                'id_cup' => 19,
                'valor_tarifa' => '51207.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 231,
                'id_cup' => 20,
                'valor_tarifa' => '16822.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 232,
                'id_cup' => 21,
                'valor_tarifa' => '41791',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 233,
                'id_cup' => 23,
                'valor_tarifa' => '25392',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 234,
                'id_cup' => 2841,
                'valor_tarifa' => '72473',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 235,
                'id_cup' => 2843,
                'valor_tarifa' => '72473',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 236,
                'id_cup' => 2850,
                'valor_tarifa' => '102731.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 237,
                'id_cup' => 2851,
                'valor_tarifa' => '102731.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 238,
                'id_cup' => 2852,
                'valor_tarifa' => '18409.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 239,
                'id_cup' => 2853,
                'valor_tarifa' => '14706.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 240,
                'id_cup' => 2856,
                'valor_tarifa' => '23170.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 241,
                'id_cup' => 2857,
                'valor_tarifa' => '20842.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 242,
                'id_cup' => 2858,
                'valor_tarifa' => '25603.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 243,
                'id_cup' => 2860,
                'valor_tarifa' => '25392',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'id' => 244,
                'id_cup' => 2861,
                'valor_tarifa' => '27931.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'id' => 245,
                'id_cup' => 2862,
                'valor_tarifa' => '69510.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'id' => 246,
                'id_cup' => 2863,
                'valor_tarifa' => '51207.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'id' => 247,
                'id_cup' => 2866,
                'valor_tarifa' => '59248',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'id' => 248,
                'id_cup' => 2885,
                'valor_tarifa' => '67288.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'id' => 249,
                'id_cup' => 2808,
                'valor_tarifa' => '13754',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'id' => 250,
                'id_cup' => 58,
                'valor_tarifa' => '49831.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'id' => 251,
                'id_cup' => 24,
                'valor_tarifa' => '104530.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'id' => 252,
                'id_cup' => 2290,
                'valor_tarifa' => '35019.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'id' => 253,
                'id_cup' => 2291,
                'valor_tarifa' => '9204.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'id' => 254,
                'id_cup' => 3767,
                'valor_tarifa' => '11849.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'id' => 255,
                'id_cup' => 93,
                'valor_tarifa' => '19467.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'id' => 256,
                'id_cup' => 122,
                'valor_tarifa' => '23699.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'id' => 257,
                'id_cup' => 25,
                'valor_tarifa' => '99452',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'id' => 258,
                'id_cup' => 25,
                'valor_tarifa' => '99452',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'id' => 259,
                'id_cup' => 25,
                'valor_tarifa' => '81042.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'id' => 260,
                'id_cup' => 3770,
                'valor_tarifa' => '99452',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'id' => 261,
                'id_cup' => 3770,
                'valor_tarifa' => '99494.32',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'id' => 262,
                'id_cup' => 3770,
                'valor_tarifa' => '174675.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'id' => 263,
                'id_cup' => 3770,
                'valor_tarifa' => '99452',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'id' => 264,
                'id_cup' => 94,
                'valor_tarifa' => '99452',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'id' => 265,
                'id_cup' => 123,
                'valor_tarifa' => '81042.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'id' => 266,
                'id_cup' => 2509,
                'valor_tarifa' => '82418.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'id' => 267,
                'id_cup' => 2509,
                'valor_tarifa' => '69510.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'id' => 268,
                'id_cup' => 2314,
                'valor_tarifa' => '270001.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'id' => 269,
                'id_cup' => 3765,
                'valor_tarifa' => '79138.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'id' => 270,
                'id_cup' => 2457,
                'valor_tarifa' => '48033.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'id' => 271,
                'id_cup' => 2458,
                'valor_tarifa' => '91093.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'id' => 272,
                'id_cup' => 3771,
                'valor_tarifa' => '46552',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'id' => 273,
                'id_cup' => 257,
                'valor_tarifa' => '30470.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'id' => 274,
                'id_cup' => 257,
                'valor_tarifa' => '70568.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'id' => 275,
                'id_cup' => 333,
                'valor_tarifa' => '58486.24',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'id' => 276,
                'id_cup' => 2293,
                'valor_tarifa' => '82418.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'id' => 277,
                'id_cup' => 2919,
                'valor_tarifa' => '108868.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'id' => 278,
                'id_cup' => 160,
                'valor_tarifa' => '25603.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'id' => 279,
                'id_cup' => 161,
                'valor_tarifa' => '66971.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'id' => 280,
                'id_cup' => 162,
                'valor_tarifa' => '108233.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'id' => 281,
                'id_cup' => 163,
                'valor_tarifa' => '108233.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'id' => 282,
                'id_cup' => 164,
                'valor_tarifa' => '15341',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'id' => 283,
                'id_cup' => 227,
                'valor_tarifa' => '33221.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'id' => 284,
                'id_cup' => 2342,
                'valor_tarifa' => '22006.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'id' => 285,
                'id_cup' => 3737,
                'valor_tarifa' => '95960.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'id' => 286,
                'id_cup' => 3738,
                'valor_tarifa' => '117120.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'id' => 287,
                'id_cup' => 71,
                'valor_tarifa' => '117120.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'id' => 288,
                'id_cup' => 72,
                'valor_tarifa' => '117120.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'id' => 289,
                'id_cup' => 74,
                'valor_tarifa' => '130663',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'id' => 290,
                'id_cup' => 78,
                'valor_tarifa' => '9098.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'id' => 291,
                'id_cup' => 2942,
                'valor_tarifa' => '74800.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'id' => 292,
                'id_cup' => 197,
                'valor_tarifa' => '80090.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'id' => 293,
                'id_cup' => 2256,
                'valor_tarifa' => '48985.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'id' => 294,
                'id_cup' => 2631,
                'valor_tarifa' => '28037',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'id' => 295,
                'id_cup' => 2632,
                'valor_tarifa' => '58295.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'id' => 296,
                'id_cup' => 2633,
                'valor_tarifa' => '60200.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'id' => 297,
                'id_cup' => 2634,
                'valor_tarifa' => '53429',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'id' => 298,
                'id_cup' => 2635,
                'valor_tarifa' => '53429',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'id' => 299,
                'id_cup' => 2636,
                'valor_tarifa' => '59777',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'id' => 300,
                'id_cup' => 2638,
                'valor_tarifa' => '32480.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'id' => 301,
                'id_cup' => 2641,
                'valor_tarifa' => '58295.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'id' => 302,
                'id_cup' => 2642,
                'valor_tarifa' => '155102.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'id' => 303,
                'id_cup' => 2643,
                'valor_tarifa' => '22747',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'id' => 304,
                'id_cup' => 2644,
                'valor_tarifa' => '96172.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'id' => 305,
                'id_cup' => 2645,
                'valor_tarifa' => '21160',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'id' => 306,
                'id_cup' => 2647,
                'valor_tarifa' => '79138.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'id' => 307,
                'id_cup' => 2648,
                'valor_tarifa' => '69510.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'id' => 308,
                'id_cup' => 2649,
                'valor_tarifa' => '43378',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'id' => 309,
                'id_cup' => 2651,
                'valor_tarifa' => '29306.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'id' => 310,
                'id_cup' => 2653,
                'valor_tarifa' => '36395.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'id' => 311,
                'id_cup' => 2661,
                'valor_tarifa' => '46446.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'id' => 312,
                'id_cup' => 3020,
                'valor_tarifa' => '29624',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'id' => 313,
                'id_cup' => 3024,
                'valor_tarifa' => '49937.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'id' => 314,
                'id_cup' => 3025,
                'valor_tarifa' => '77234',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'id' => 315,
                'id_cup' => 3026,
                'valor_tarifa' => '8252.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'id' => 316,
                'id_cup' => 3027,
                'valor_tarifa' => '93104',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'id' => 317,
                'id_cup' => 3028,
                'valor_tarifa' => '70674.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'id' => 318,
                'id_cup' => 3031,
                'valor_tarifa' => '75541.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'id' => 319,
                'id_cup' => 3033,
                'valor_tarifa' => '182505',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'id' => 320,
                'id_cup' => 3034,
                'valor_tarifa' => '14600.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'id' => 321,
                'id_cup' => 2662,
                'valor_tarifa' => '31528.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'id' => 322,
                'id_cup' => 3037,
                'valor_tarifa' => '28777.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'id' => 323,
                'id_cup' => 3038,
                'valor_tarifa' => '55968.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'id' => 324,
                'id_cup' => 3039,
                'valor_tarifa' => '46552',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'id' => 325,
                'id_cup' => 3040,
                'valor_tarifa' => '50995.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'id' => 326,
                'id_cup' => 3044,
                'valor_tarifa' => '11003.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'id' => 327,
                'id_cup' => 3046,
                'valor_tarifa' => '16716.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'id' => 328,
                'id_cup' => 3047,
                'valor_tarifa' => '16716.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'id' => 329,
                'id_cup' => 3048,
                'valor_tarifa' => '75329.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'id' => 330,
                'id_cup' => 3055,
                'valor_tarifa' => '42743.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'id' => 331,
                'id_cup' => 3058,
                'valor_tarifa' => '55121.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'id' => 332,
                'id_cup' => 3059,
                'valor_tarifa' => '18620.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'id' => 333,
                'id_cup' => 3060,
                'valor_tarifa' => '11109',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'id' => 334,
                'id_cup' => 3063,
                'valor_tarifa' => '39251.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'id' => 335,
                'id_cup' => 3066,
                'valor_tarifa' => '39251.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'id' => 336,
                'id_cup' => 3067,
                'valor_tarifa' => '9627.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'id' => 337,
                'id_cup' => 3068,
                'valor_tarifa' => '110455.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'id' => 338,
                'id_cup' => 2666,
                'valor_tarifa' => '14790.84',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'id' => 339,
                'id_cup' => 2667,
                'valor_tarifa' => '44118.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'id' => 340,
                'id_cup' => 2669,
                'valor_tarifa' => '42002.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'id' => 341,
                'id_cup' => 2670,
                'valor_tarifa' => '46552',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'id' => 342,
                'id_cup' => 3070,
                'valor_tarifa' => '27931.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'id' => 343,
                'id_cup' => 3071,
                'valor_tarifa' => '32798',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'id' => 344,
                'id_cup' => 3073,
                'valor_tarifa' => '87708.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'id' => 345,
                'id_cup' => 3074,
                'valor_tarifa' => '56708.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'id' => 346,
                'id_cup' => 3075,
                'valor_tarifa' => '77234',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'id' => 347,
                'id_cup' => 3076,
                'valor_tarifa' => '84111',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'id' => 348,
                'id_cup' => 3776,
                'valor_tarifa' => '74800.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'id' => 349,
                'id_cup' => 3077,
                'valor_tarifa' => '60306',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'id' => 350,
                'id_cup' => 3081,
                'valor_tarifa' => '50995.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'id' => 351,
                'id_cup' => 3083,
                'valor_tarifa' => '57978.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'id' => 352,
                'id_cup' => 3084,
                'valor_tarifa' => '46552',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'id' => 353,
                'id_cup' => 3085,
                'valor_tarifa' => '13754',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'id' => 354,
                'id_cup' => 3086,
                'valor_tarifa' => '11638',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'id' => 355,
                'id_cup' => 3087,
                'valor_tarifa' => '58295.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'id' => 356,
                'id_cup' => 3090,
                'valor_tarifa' => '44541.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'id' => 357,
                'id_cup' => 3091,
                'valor_tarifa' => '9299.82',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'id' => 358,
                'id_cup' => 3092,
                'valor_tarifa' => '10474.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'id' => 359,
                'id_cup' => 3099,
                'valor_tarifa' => '10474.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'id' => 360,
                'id_cup' => 3101,
                'valor_tarifa' => '39251.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'id' => 361,
                'id_cup' => 3103,
                'valor_tarifa' => '13436.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'id' => 362,
                'id_cup' => 3104,
                'valor_tarifa' => '33432.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'id' => 363,
                'id_cup' => 3105,
                'valor_tarifa' => '39040.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'id' => 364,
                'id_cup' => 3105,
                'valor_tarifa' => '37135.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'id' => 365,
                'id_cup' => 3106,
                'valor_tarifa' => '776794.18',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'id' => 366,
                'id_cup' => 3107,
                'valor_tarifa' => '37199.28',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'id' => 367,
                'id_cup' => 3108,
                'valor_tarifa' => '35337.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'id' => 368,
                'id_cup' => 3109,
                'valor_tarifa' => '90564.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'id' => 369,
                'id_cup' => 3113,
                'valor_tarifa' => '25392',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'id' => 370,
                'id_cup' => 3114,
                'valor_tarifa' => '84005.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'id' => 371,
                'id_cup' => 3116,
                'valor_tarifa' => '14600.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'id' => 372,
                'id_cup' => 3118,
                'valor_tarifa' => '82418.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'id' => 373,
                'id_cup' => 3119,
                'valor_tarifa' => '76281.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'id' => 374,
                'id_cup' => 3120,
                'valor_tarifa' => '80111.76',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'id' => 375,
                'id_cup' => 3124,
                'valor_tarifa' => '56708.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'id' => 376,
                'id_cup' => 3125,
                'valor_tarifa' => '7617.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'id' => 377,
                'id_cup' => 3126,
                'valor_tarifa' => '5829.58',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'id' => 378,
                'id_cup' => 3127,
                'valor_tarifa' => '5713.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'id' => 379,
                'id_cup' => 3128,
                'valor_tarifa' => '37770.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'id' => 380,
                'id_cup' => 3129,
                'valor_tarifa' => '30787.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'id' => 381,
                'id_cup' => 3130,
                'valor_tarifa' => '79032.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'id' => 382,
                'id_cup' => 3131,
                'valor_tarifa' => '104530.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'id' => 383,
                'id_cup' => 3132,
                'valor_tarifa' => '384583',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'id' => 384,
                'id_cup' => 3133,
                'valor_tarifa' => '46552',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'id' => 385,
                'id_cup' => 3134,
                'valor_tarifa' => '14600.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'id' => 386,
                'id_cup' => 3135,
                'valor_tarifa' => '55333.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'id' => 387,
                'id_cup' => 3136,
                'valor_tarifa' => '27000.16',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'id' => 388,
                'id_cup' => 3137,
                'valor_tarifa' => '137434.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'id' => 389,
                'id_cup' => 3138,
                'valor_tarifa' => '30470.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'id' => 390,
                'id_cup' => 3140,
                'valor_tarifa' => '46552',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'id' => 391,
                'id_cup' => 3141,
                'valor_tarifa' => '48350.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'id' => 392,
                'id_cup' => 3142,
                'valor_tarifa' => '80619.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'id' => 393,
                'id_cup' => 3145,
                'valor_tarifa' => '79138.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'id' => 394,
                'id_cup' => 3147,
                'valor_tarifa' => '19498.94',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'id' => 395,
                'id_cup' => 3150,
                'valor_tarifa' => '72473',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'id' => 396,
                'id_cup' => 3151,
                'valor_tarifa' => '155420.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'id' => 397,
                'id_cup' => 3153,
                'valor_tarifa' => '72261.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'id' => 398,
                'id_cup' => 3154,
                'valor_tarifa' => '26026.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'id' => 399,
                'id_cup' => 3155,
                'valor_tarifa' => '35548.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'id' => 400,
                'id_cup' => 3158,
                'valor_tarifa' => '39675',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'id' => 401,
                'id_cup' => 3159,
                'valor_tarifa' => '111830.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'id' => 402,
                'id_cup' => 3160,
                'valor_tarifa' => '84005.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'id' => 403,
                'id_cup' => 3161,
                'valor_tarifa' => '24016.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'id' => 404,
                'id_cup' => 3164,
                'valor_tarifa' => '24016.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'id' => 405,
                'id_cup' => 3168,
                'valor_tarifa' => '15446.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'id' => 406,
                'id_cup' => 3169,
                'valor_tarifa' => '34702.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'id' => 407,
                'id_cup' => 3170,
                'valor_tarifa' => '24334',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'id' => 408,
                'id_cup' => 3172,
                'valor_tarifa' => '53746.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'id' => 409,
                'id_cup' => 3174,
                'valor_tarifa' => '10156.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'id' => 410,
                'id_cup' => 3175,
                'valor_tarifa' => '31422.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'id' => 411,
                'id_cup' => 3178,
                'valor_tarifa' => '70674.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'id' => 412,
                'id_cup' => 3179,
                'valor_tarifa' => '237732.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'id' => 413,
                'id_cup' => 3181,
                'valor_tarifa' => '14706.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'id' => 414,
                'id_cup' => 3184,
                'valor_tarifa' => '28671.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'id' => 415,
                'id_cup' => 3187,
                'valor_tarifa' => '121141',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'id' => 416,
                'id_cup' => 3188,
                'valor_tarifa' => '49408.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'id' => 417,
                'id_cup' => 3191,
                'valor_tarifa' => '97812.1',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'id' => 418,
                'id_cup' => 3193,
                'valor_tarifa' => '200173.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'id' => 419,
                'id_cup' => 3194,
                'valor_tarifa' => '155314.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'id' => 420,
                'id_cup' => 3195,
                'valor_tarifa' => '410292.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'id' => 421,
                'id_cup' => 3196,
                'valor_tarifa' => '271694.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'id' => 422,
                'id_cup' => 3197,
                'valor_tarifa' => '300154.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'id' => 423,
                'id_cup' => 3199,
                'valor_tarifa' => '149495.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'id' => 424,
                'id_cup' => 3200,
                'valor_tarifa' => '396538.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'id' => 425,
                'id_cup' => 3202,
                'valor_tarifa' => '177849.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'id' => 426,
                'id_cup' => 3203,
                'valor_tarifa' => '26238.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'id' => 427,
                'id_cup' => 3204,
                'valor_tarifa' => '26344.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'id' => 428,
                'id_cup' => 3205,
                'valor_tarifa' => '57872.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'id' => 429,
                'id_cup' => 3206,
                'valor_tarifa' => '63585.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'id' => 430,
                'id_cup' => 3207,
                'valor_tarifa' => '198269.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'id' => 431,
                'id_cup' => 3208,
                'valor_tarifa' => '592903.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'id' => 432,
                'id_cup' => 3209,
                'valor_tarifa' => '59988.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'id' => 433,
                'id_cup' => 3211,
                'valor_tarifa' => '73848.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'id' => 434,
                'id_cup' => 3214,
                'valor_tarifa' => '51207.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'id' => 435,
                'id_cup' => 3217,
                'valor_tarifa' => '86756',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'id' => 436,
                'id_cup' => 3218,
                'valor_tarifa' => '24545.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'id' => 437,
                'id_cup' => 3219,
                'valor_tarifa' => '69616.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'id' => 438,
                'id_cup' => 3221,
                'valor_tarifa' => '97441.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'id' => 439,
                'id_cup' => 3222,
                'valor_tarifa' => '210118.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'id' => 440,
                'id_cup' => 3223,
                'valor_tarifa' => '239742.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'id' => 441,
                'id_cup' => 3225,
                'valor_tarifa' => '60306',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'id' => 442,
                'id_cup' => 3227,
                'valor_tarifa' => '68029.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            442 => 
            array (
                'id' => 443,
                'id_cup' => 3229,
                'valor_tarifa' => '95008.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            443 => 
            array (
                'id' => 444,
                'id_cup' => 3230,
                'valor_tarifa' => '77974.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            444 => 
            array (
                'id' => 445,
                'id_cup' => 3231,
                'valor_tarifa' => '86756',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            445 => 
            array (
                'id' => 446,
                'id_cup' => 3233,
                'valor_tarifa' => '579678.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            446 => 
            array (
                'id' => 447,
                'id_cup' => 3235,
                'valor_tarifa' => '253708.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            447 => 
            array (
                'id' => 448,
                'id_cup' => 3239,
                'valor_tarifa' => '225036.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            448 => 
            array (
                'id' => 449,
                'id_cup' => 3240,
                'valor_tarifa' => '194883.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            449 => 
            array (
                'id' => 450,
                'id_cup' => 3243,
                'valor_tarifa' => '194883.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            450 => 
            array (
                'id' => 451,
                'id_cup' => 3245,
                'valor_tarifa' => '172983',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            451 => 
            array (
                'id' => 452,
                'id_cup' => 3248,
                'valor_tarifa' => '82418.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            452 => 
            array (
                'id' => 453,
                'id_cup' => 3249,
                'valor_tarifa' => '52476.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            453 => 
            array (
                'id' => 454,
                'id_cup' => 3250,
                'valor_tarifa' => '76281.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            454 => 
            array (
                'id' => 455,
                'id_cup' => 3251,
                'valor_tarifa' => '74694.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            455 => 
            array (
                'id' => 456,
                'id_cup' => 3252,
                'valor_tarifa' => '93104',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            456 => 
            array (
                'id' => 457,
                'id_cup' => 3253,
                'valor_tarifa' => '72578.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            457 => 
            array (
                'id' => 458,
                'id_cup' => 3254,
                'valor_tarifa' => '125584.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            458 => 
            array (
                'id' => 459,
                'id_cup' => 3255,
                'valor_tarifa' => '34173.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            459 => 
            array (
                'id' => 460,
                'id_cup' => 3256,
                'valor_tarifa' => '97547.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            460 => 
            array (
                'id' => 461,
                'id_cup' => 3258,
                'valor_tarifa' => '66125',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            461 => 
            array (
                'id' => 462,
                'id_cup' => 3259,
                'valor_tarifa' => '44150.34',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            462 => 
            array (
                'id' => 463,
                'id_cup' => 3260,
                'valor_tarifa' => '59882.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            463 => 
            array (
                'id' => 464,
                'id_cup' => 3261,
                'valor_tarifa' => '69722.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            464 => 
            array (
                'id' => 465,
                'id_cup' => 3262,
                'valor_tarifa' => '221862.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            465 => 
            array (
                'id' => 466,
                'id_cup' => 3263,
                'valor_tarifa' => '268308.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            466 => 
            array (
                'id' => 467,
                'id_cup' => 3264,
                'valor_tarifa' => '235087.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            467 => 
            array (
                'id' => 468,
                'id_cup' => 3266,
                'valor_tarifa' => '193719.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            468 => 
            array (
                'id' => 469,
                'id_cup' => 3267,
                'valor_tarifa' => '229691.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            469 => 
            array (
                'id' => 470,
                'id_cup' => 3268,
                'valor_tarifa' => '158911.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            470 => 
            array (
                'id' => 471,
                'id_cup' => 3269,
                'valor_tarifa' => '210076.48',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            471 => 
            array (
                'id' => 472,
                'id_cup' => 3270,
                'valor_tarifa' => '77128.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            472 => 
            array (
                'id' => 473,
                'id_cup' => 3271,
                'valor_tarifa' => '102520.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            473 => 
            array (
                'id' => 474,
                'id_cup' => 3272,
                'valor_tarifa' => '166106',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            474 => 
            array (
                'id' => 475,
                'id_cup' => 3273,
                'valor_tarifa' => '138280.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            475 => 
            array (
                'id' => 476,
                'id_cup' => 3274,
                'valor_tarifa' => '116168.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            476 => 
            array (
                'id' => 477,
                'id_cup' => 3275,
                'valor_tarifa' => '116295.36',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            477 => 
            array (
                'id' => 478,
                'id_cup' => 3276,
                'valor_tarifa' => '82460.52',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            478 => 
            array (
                'id' => 479,
                'id_cup' => 3277,
                'valor_tarifa' => '149241.48',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            479 => 
            array (
                'id' => 480,
                'id_cup' => 3278,
                'valor_tarifa' => '162508.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            480 => 
            array (
                'id' => 481,
                'id_cup' => 3279,
                'valor_tarifa' => '143570.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            481 => 
            array (
                'id' => 482,
                'id_cup' => 3280,
                'valor_tarifa' => '85486.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            482 => 
            array (
                'id' => 483,
                'id_cup' => 3281,
                'valor_tarifa' => '290738.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            483 => 
            array (
                'id' => 484,
                'id_cup' => 3282,
                'valor_tarifa' => '180812.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            484 => 
            array (
                'id' => 485,
                'id_cup' => 3284,
                'valor_tarifa' => '140290.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            485 => 
            array (
                'id' => 486,
                'id_cup' => 3286,
                'valor_tarifa' => '172242.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            486 => 
            array (
                'id' => 487,
                'id_cup' => 3288,
                'valor_tarifa' => '154573.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            487 => 
            array (
                'id' => 488,
                'id_cup' => 3289,
                'valor_tarifa' => '174146.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            488 => 
            array (
                'id' => 489,
                'id_cup' => 3289,
                'valor_tarifa' => '174146.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            489 => 
            array (
                'id' => 490,
                'id_cup' => 3290,
                'valor_tarifa' => '116168.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            490 => 
            array (
                'id' => 491,
                'id_cup' => 3291,
                'valor_tarifa' => '116168.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            491 => 
            array (
                'id' => 492,
                'id_cup' => 3293,
                'valor_tarifa' => '116168.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            492 => 
            array (
                'id' => 493,
                'id_cup' => 3294,
                'valor_tarifa' => '116168.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            493 => 
            array (
                'id' => 494,
                'id_cup' => 3295,
                'valor_tarifa' => '131932.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            494 => 
            array (
                'id' => 495,
                'id_cup' => 3296,
                'valor_tarifa' => '144311.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            495 => 
            array (
                'id' => 496,
                'id_cup' => 3298,
                'valor_tarifa' => '454093.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            496 => 
            array (
                'id' => 497,
                'id_cup' => 3299,
                'valor_tarifa' => '908293',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            497 => 
            array (
                'id' => 498,
                'id_cup' => 3300,
                'valor_tarifa' => '243022.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            498 => 
            array (
                'id' => 499,
                'id_cup' => 3301,
                'valor_tarifa' => '159758',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            499 => 
            array (
                'id' => 500,
                'id_cup' => 3302,
                'valor_tarifa' => '160392.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        \DB::table('tbl_cups_tarifa')->insert(array (
            0 => 
            array (
                'id' => 501,
                'id_cup' => 3303,
                'valor_tarifa' => '172136.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 502,
                'id_cup' => 3304,
                'valor_tarifa' => '354218.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 503,
                'id_cup' => 3305,
                'valor_tarifa' => '605281.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 504,
                'id_cup' => 3309,
                'valor_tarifa' => '190122.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 505,
                'id_cup' => 3313,
                'valor_tarifa' => '426479.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 506,
                'id_cup' => 3314,
                'valor_tarifa' => '154468',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 507,
                'id_cup' => 3315,
                'valor_tarifa' => '426479.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 508,
                'id_cup' => 3317,
                'valor_tarifa' => '82418.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 509,
                'id_cup' => 3318,
                'valor_tarifa' => '97653.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 510,
                'id_cup' => 3319,
                'valor_tarifa' => '102520.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 511,
                'id_cup' => 3320,
                'valor_tarifa' => '558983.72',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 512,
                'id_cup' => 3324,
                'valor_tarifa' => '454093.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 513,
                'id_cup' => 3327,
                'valor_tarifa' => '232760',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 514,
                'id_cup' => 3329,
                'valor_tarifa' => '413889.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 515,
                'id_cup' => 3332,
                'valor_tarifa' => '854430.22',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 516,
                'id_cup' => 3333,
                'valor_tarifa' => '140185',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'id' => 517,
                'id_cup' => 3334,
                'valor_tarifa' => '479379.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'id' => 518,
                'id_cup' => 3335,
                'valor_tarifa' => '508898',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'id' => 519,
                'id_cup' => 3336,
                'valor_tarifa' => '454326.36',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'id' => 520,
                'id_cup' => 3337,
                'valor_tarifa' => '121225.64',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'id' => 521,
                'id_cup' => 3338,
                'valor_tarifa' => '103155',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'id' => 522,
                'id_cup' => 3339,
                'valor_tarifa' => '102742.38',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'id' => 523,
                'id_cup' => 3342,
                'valor_tarifa' => '289183.14',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'id' => 524,
                'id_cup' => 3343,
                'valor_tarifa' => '409869.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'id' => 525,
                'id_cup' => 3346,
                'valor_tarifa' => '579360.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'id' => 526,
                'id_cup' => 3347,
                'valor_tarifa' => '506147.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'id' => 527,
                'id_cup' => 3349,
                'valor_tarifa' => '126854.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'id' => 528,
                'id_cup' => 3350,
                'valor_tarifa' => '164307.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'id' => 529,
                'id_cup' => 3351,
                'valor_tarifa' => '982353',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'id' => 530,
                'id_cup' => 3352,
                'valor_tarifa' => '1663493.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'id' => 531,
                'id_cup' => 3353,
                'valor_tarifa' => '1062866.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'id' => 532,
                'id_cup' => 3355,
                'valor_tarifa' => '105546.08',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'id' => 533,
                'id_cup' => 3357,
                'valor_tarifa' => '141772',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'id' => 534,
                'id_cup' => 3361,
                'valor_tarifa' => '156160.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'id' => 535,
                'id_cup' => 3364,
                'valor_tarifa' => '156160.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'id' => 536,
                'id_cup' => 3365,
                'valor_tarifa' => '44541.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'id' => 537,
                'id_cup' => 3367,
                'valor_tarifa' => '265981.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'id' => 538,
                'id_cup' => 3368,
                'valor_tarifa' => '190122.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'id' => 539,
                'id_cup' => 3369,
                'valor_tarifa' => '319621.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'id' => 540,
                'id_cup' => 3371,
                'valor_tarifa' => '245456',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'id' => 541,
                'id_cup' => 3372,
                'valor_tarifa' => '228528',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'id' => 542,
                'id_cup' => 3375,
                'valor_tarifa' => '371675.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'id' => 543,
                'id_cup' => 3376,
                'valor_tarifa' => '204511.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'id' => 544,
                'id_cup' => 3377,
                'valor_tarifa' => '303540.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'id' => 545,
                'id_cup' => 3378,
                'valor_tarifa' => '619988',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'id' => 546,
                'id_cup' => 3380,
                'valor_tarifa' => '268097.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'id' => 547,
                'id_cup' => 3382,
                'valor_tarifa' => '203770.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'id' => 548,
                'id_cup' => 3383,
                'valor_tarifa' => '249370.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'id' => 549,
                'id_cup' => 3384,
                'valor_tarifa' => '285660',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'id' => 550,
                'id_cup' => 3386,
                'valor_tarifa' => '375061',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'id' => 551,
                'id_cup' => 3387,
                'valor_tarifa' => '375061',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'id' => 552,
                'id_cup' => 3389,
                'valor_tarifa' => '513764.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'id' => 553,
                'id_cup' => 3390,
                'valor_tarifa' => '7403249.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'id' => 554,
                'id_cup' => 3391,
                'valor_tarifa' => '1384710.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'id' => 555,
                'id_cup' => 3777,
                'valor_tarifa' => '181658.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'id' => 556,
                'id_cup' => 3780,
                'valor_tarifa' => '338771.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'id' => 557,
                'id_cup' => 3783,
                'valor_tarifa' => '319621.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'id' => 558,
                'id_cup' => 3786,
                'valor_tarifa' => '529740.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'id' => 559,
                'id_cup' => 3787,
                'valor_tarifa' => '307560.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'id' => 560,
                'id_cup' => 3788,
                'valor_tarifa' => '212658',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'id' => 561,
                'id_cup' => 3789,
                'valor_tarifa' => '225354',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'id' => 562,
                'id_cup' => 3790,
                'valor_tarifa' => '193402.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'id' => 563,
                'id_cup' => 3792,
                'valor_tarifa' => '234241.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'id' => 564,
                'id_cup' => 3794,
                'valor_tarifa' => '651939.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'id' => 565,
                'id_cup' => 3796,
                'valor_tarifa' => '610148.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'id' => 566,
                'id_cup' => 3798,
                'valor_tarifa' => '746429.58',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'id' => 567,
                'id_cup' => 3799,
                'valor_tarifa' => '1071912.7',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'id' => 568,
                'id_cup' => 3394,
                'valor_tarifa' => '1747974.7',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'id' => 569,
                'id_cup' => 3395,
                'valor_tarifa' => '610148.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'id' => 570,
                'id_cup' => 3405,
                'valor_tarifa' => '749593',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'id' => 571,
                'id_cup' => 3412,
                'valor_tarifa' => '29412.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'id' => 572,
                'id_cup' => 3413,
                'valor_tarifa' => '48350.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'id' => 573,
                'id_cup' => 3416,
                'valor_tarifa' => '60940.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'id' => 574,
                'id_cup' => 3417,
                'valor_tarifa' => '267779.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'id' => 575,
                'id_cup' => 3419,
                'valor_tarifa' => '65659.48',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'id' => 576,
                'id_cup' => 3421,
                'valor_tarifa' => '55121.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'id' => 577,
                'id_cup' => 3422,
                'valor_tarifa' => '9488673',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'id' => 578,
                'id_cup' => 3424,
                'valor_tarifa' => '7908655.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'id' => 579,
                'id_cup' => 3425,
                'valor_tarifa' => '60094.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'id' => 580,
                'id_cup' => 3426,
                'valor_tarifa' => '50646.46',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'id' => 581,
                'id_cup' => 3427,
                'valor_tarifa' => '53217.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'id' => 582,
                'id_cup' => 3433,
                'valor_tarifa' => '179331',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'id' => 583,
                'id_cup' => 3434,
                'valor_tarifa' => '179331',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'id' => 584,
                'id_cup' => 3435,
                'valor_tarifa' => '107281.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'id' => 585,
                'id_cup' => 3436,
                'valor_tarifa' => '57555.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'id' => 586,
                'id_cup' => 3438,
                'valor_tarifa' => '57026.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'id' => 587,
                'id_cup' => 3439,
                'valor_tarifa' => '45070.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'id' => 588,
                'id_cup' => 3446,
                'valor_tarifa' => '202924.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'id' => 589,
                'id_cup' => 3447,
                'valor_tarifa' => '397808',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'id' => 590,
                'id_cup' => 3448,
                'valor_tarifa' => '440551.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'id' => 591,
                'id_cup' => 3449,
                'valor_tarifa' => '568146',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'id' => 592,
                'id_cup' => 3450,
                'valor_tarifa' => '560422.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'id' => 593,
                'id_cup' => 3451,
                'valor_tarifa' => '455447.84',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'id' => 594,
                'id_cup' => 3453,
                'valor_tarifa' => '1304831.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'id' => 595,
                'id_cup' => 3454,
                'valor_tarifa' => '1741700.76',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'id' => 596,
                'id_cup' => 3456,
                'valor_tarifa' => '80619.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'id' => 597,
                'id_cup' => 3457,
                'valor_tarifa' => '158911.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'id' => 598,
                'id_cup' => 3458,
                'valor_tarifa' => '13754',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'id' => 599,
                'id_cup' => 3459,
                'valor_tarifa' => '23170.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'id' => 600,
                'id_cup' => 3460,
                'valor_tarifa' => '55862.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'id' => 601,
                'id_cup' => 3461,
                'valor_tarifa' => '55862.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'id' => 602,
                'id_cup' => 3468,
                'valor_tarifa' => '60168.46',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'id' => 603,
                'id_cup' => 3470,
                'valor_tarifa' => '59988.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'id' => 604,
                'id_cup' => 3473,
                'valor_tarifa' => '21160',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'id' => 605,
                'id_cup' => 3475,
                'valor_tarifa' => '21160',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'id' => 606,
                'id_cup' => 3476,
                'valor_tarifa' => '21300',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'id' => 607,
                'id_cup' => 3477,
                'valor_tarifa' => '21160',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'id' => 608,
                'id_cup' => 3482,
                'valor_tarifa' => '72473',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'id' => 609,
                'id_cup' => 3483,
                'valor_tarifa' => '113311.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'id' => 610,
                'id_cup' => 3484,
                'valor_tarifa' => '113206',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'id' => 611,
                'id_cup' => 3491,
                'valor_tarifa' => '148225.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'id' => 612,
                'id_cup' => 3492,
                'valor_tarifa' => '113206',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'id' => 613,
                'id_cup' => 3493,
                'valor_tarifa' => '107175.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'id' => 614,
                'id_cup' => 3494,
                'valor_tarifa' => '107175.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'id' => 615,
                'id_cup' => 3495,
                'valor_tarifa' => '107175.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'id' => 616,
                'id_cup' => 3497,
                'valor_tarifa' => '214033.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'id' => 617,
                'id_cup' => 3499,
                'valor_tarifa' => '88343',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'id' => 618,
                'id_cup' => 3502,
                'valor_tarifa' => '75435.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'id' => 619,
                'id_cup' => 3504,
                'valor_tarifa' => '110984.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'id' => 620,
                'id_cup' => 3507,
                'valor_tarifa' => '116274.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'id' => 621,
                'id_cup' => 3508,
                'valor_tarifa' => '47715.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'id' => 622,
                'id_cup' => 3509,
                'valor_tarifa' => '23170.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'id' => 623,
                'id_cup' => 3510,
                'valor_tarifa' => '44859.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'id' => 624,
                'id_cup' => 3511,
                'valor_tarifa' => '20419.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'id' => 625,
                'id_cup' => 3512,
                'valor_tarifa' => '59600',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'id' => 626,
                'id_cup' => 3513,
                'valor_tarifa' => '24000',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'id' => 627,
                'id_cup' => 3516,
                'valor_tarifa' => '112571.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'id' => 628,
                'id_cup' => 3517,
                'valor_tarifa' => '1199900',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'id' => 629,
                'id_cup' => 3519,
                'valor_tarifa' => '22600',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'id' => 630,
                'id_cup' => 3520,
                'valor_tarifa' => '24439.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'id' => 631,
                'id_cup' => 3521,
                'valor_tarifa' => '10156.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'id' => 632,
                'id_cup' => 3522,
                'valor_tarifa' => '23200',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'id' => 633,
                'id_cup' => 3524,
                'valor_tarifa' => '40000',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'id' => 634,
                'id_cup' => 3527,
                'valor_tarifa' => '29500',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'id' => 635,
                'id_cup' => 3531,
                'valor_tarifa' => '16081.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'id' => 636,
                'id_cup' => 3534,
                'valor_tarifa' => '164200',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'id' => 637,
                'id_cup' => 3536,
                'valor_tarifa' => '91728.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'id' => 638,
                'id_cup' => 3537,
                'valor_tarifa' => '55862.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'id' => 639,
                'id_cup' => 3539,
                'valor_tarifa' => '77600',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'id' => 640,
                'id_cup' => 3540,
                'valor_tarifa' => '77600',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'id' => 641,
                'id_cup' => 3545,
                'valor_tarifa' => '97200',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'id' => 642,
                'id_cup' => 3547,
                'valor_tarifa' => '20948.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'id' => 643,
                'id_cup' => 3548,
                'valor_tarifa' => '25700',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'id' => 644,
                'id_cup' => 3549,
                'valor_tarifa' => '97200',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'id' => 645,
                'id_cup' => 3553,
                'valor_tarifa' => '15446.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'id' => 646,
                'id_cup' => 3554,
                'valor_tarifa' => '19600',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'id' => 647,
                'id_cup' => 3555,
                'valor_tarifa' => '7723.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'id' => 648,
                'id_cup' => 3556,
                'valor_tarifa' => '51524.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'id' => 649,
                'id_cup' => 3557,
                'valor_tarifa' => '9839.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'id' => 650,
                'id_cup' => 3558,
                'valor_tarifa' => '48403.5',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'id' => 651,
                'id_cup' => 3559,
                'valor_tarifa' => '24757.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'id' => 652,
                'id_cup' => 3562,
                'valor_tarifa' => '106858',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'id' => 653,
                'id_cup' => 3564,
                'valor_tarifa' => '52900',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'id' => 654,
                'id_cup' => 3565,
                'valor_tarifa' => '51736.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'id' => 655,
                'id_cup' => 3567,
                'valor_tarifa' => '77128.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'id' => 656,
                'id_cup' => 3568,
                'valor_tarifa' => '51736.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'id' => 657,
                'id_cup' => 3569,
                'valor_tarifa' => '77128.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'id' => 658,
                'id_cup' => 3570,
                'valor_tarifa' => '51736.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'id' => 659,
                'id_cup' => 3571,
                'valor_tarifa' => '60094.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'id' => 660,
                'id_cup' => 3572,
                'valor_tarifa' => '48456.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'id' => 661,
                'id_cup' => 3574,
                'valor_tarifa' => '144311.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'id' => 662,
                'id_cup' => 3577,
                'valor_tarifa' => '60094.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'id' => 663,
                'id_cup' => 3580,
                'valor_tarifa' => '28121.64',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'id' => 664,
                'id_cup' => 3582,
                'valor_tarifa' => '111724.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'id' => 665,
                'id_cup' => 3585,
                'valor_tarifa' => '179828.26',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'id' => 666,
                'id_cup' => 3585,
                'valor_tarifa' => '168645.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'id' => 667,
                'id_cup' => 3586,
                'valor_tarifa' => '136693.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'id' => 668,
                'id_cup' => 3588,
                'valor_tarifa' => '125595.18',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'id' => 669,
                'id_cup' => 3589,
                'valor_tarifa' => '254554.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'id' => 670,
                'id_cup' => 3590,
                'valor_tarifa' => '231596.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'id' => 671,
                'id_cup' => 3598,
                'valor_tarifa' => '124949.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'id' => 672,
                'id_cup' => 3599,
                'valor_tarifa' => '196788',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'id' => 673,
                'id_cup' => 3601,
                'valor_tarifa' => '470492.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'id' => 674,
                'id_cup' => 3605,
                'valor_tarifa' => '734463.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'id' => 675,
                'id_cup' => 3607,
                'valor_tarifa' => '177744',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'id' => 676,
                'id_cup' => 3609,
                'valor_tarifa' => '63268.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'id' => 677,
                'id_cup' => 3613,
                'valor_tarifa' => '99134.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'id' => 678,
                'id_cup' => 3615,
                'valor_tarifa' => '229131.06',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'id' => 679,
                'id_cup' => 3616,
                'valor_tarifa' => '38828.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'id' => 680,
                'id_cup' => 3617,
                'valor_tarifa' => '55333.4',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'id' => 681,
                'id_cup' => 3618,
                'valor_tarifa' => '47715.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'id' => 682,
                'id_cup' => 3619,
                'valor_tarifa' => '52582.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'id' => 683,
                'id_cup' => 3620,
                'valor_tarifa' => '33100',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'id' => 684,
                'id_cup' => 3621,
                'valor_tarifa' => '47726.38',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'id' => 685,
                'id_cup' => 3622,
                'valor_tarifa' => '40733',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'id' => 686,
                'id_cup' => 3623,
                'valor_tarifa' => '47610',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'id' => 687,
                'id_cup' => 3625,
                'valor_tarifa' => '96172.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'id' => 688,
                'id_cup' => 3626,
                'valor_tarifa' => '54169.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'id' => 689,
                'id_cup' => 3628,
                'valor_tarifa' => '57481.14',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'id' => 690,
                'id_cup' => 3629,
                'valor_tarifa' => '112571.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'id' => 691,
                'id_cup' => 3630,
                'valor_tarifa' => '232760',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'id' => 692,
                'id_cup' => 3630,
                'valor_tarifa' => '108445',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'id' => 693,
                'id_cup' => 3631,
                'valor_tarifa' => '773768.3',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'id' => 694,
                'id_cup' => 3632,
                'valor_tarifa' => '45155.44',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'id' => 695,
                'id_cup' => 3633,
                'valor_tarifa' => '99134.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'id' => 696,
                'id_cup' => 3634,
                'valor_tarifa' => '19498.94',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'id' => 697,
                'id_cup' => 3635,
                'valor_tarifa' => '19573',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'id' => 698,
                'id_cup' => 3636,
                'valor_tarifa' => '164519',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'id' => 699,
                'id_cup' => 3639,
                'valor_tarifa' => '209632.12',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'id' => 700,
                'id_cup' => 3641,
                'valor_tarifa' => '467445.56',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'id' => 701,
                'id_cup' => 3642,
                'valor_tarifa' => '467318.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'id' => 702,
                'id_cup' => 3643,
                'valor_tarifa' => '64000',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'id' => 703,
                'id_cup' => 3644,
                'valor_tarifa' => '10262.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'id' => 704,
                'id_cup' => 3645,
                'valor_tarifa' => '15446.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'id' => 705,
                'id_cup' => 3645,
                'valor_tarifa' => '27300',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'id' => 706,
                'id_cup' => 3646,
                'valor_tarifa' => '7901440.24',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'id' => 707,
                'id_cup' => 3647,
                'valor_tarifa' => '2822458.34',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'id' => 708,
                'id_cup' => 3648,
                'valor_tarifa' => '2582239.44',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'id' => 709,
                'id_cup' => 3649,
                'valor_tarifa' => '2510528.2',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'id' => 710,
                'id_cup' => 3650,
                'valor_tarifa' => '6855194.62',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'id' => 711,
                'id_cup' => 3651,
                'valor_tarifa' => '2357932.86',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'id' => 712,
                'id_cup' => 3652,
                'valor_tarifa' => '7362071.84',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'id' => 713,
                'id_cup' => 3653,
                'valor_tarifa' => '229014.68',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'id' => 714,
                'id_cup' => 3654,
                'valor_tarifa' => '5850507.24',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'id' => 715,
                'id_cup' => 3655,
                'valor_tarifa' => '3131912.76',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'id' => 716,
                'id_cup' => 3656,
                'valor_tarifa' => '4628580.72',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'id' => 717,
                'id_cup' => 3658,
                'valor_tarifa' => '2517479.26',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'id' => 718,
                'id_cup' => 3659,
                'valor_tarifa' => '2924417.8',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'id' => 719,
                'id_cup' => 3660,
                'valor_tarifa' => '2064485.98',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'id' => 720,
                'id_cup' => 3661,
                'valor_tarifa' => '9327624.24',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'id' => 721,
                'id_cup' => 3662,
                'valor_tarifa' => '3555440.74',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'id' => 722,
                'id_cup' => 3663,
                'valor_tarifa' => '2908050.54',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'id' => 723,
                'id_cup' => 3664,
                'valor_tarifa' => '6436374.74',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'id' => 724,
                'id_cup' => 3665,
                'valor_tarifa' => '13000259.64',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'id' => 725,
                'id_cup' => 3675,
                'valor_tarifa' => '4393059.34',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'id' => 726,
                'id_cup' => 3676,
                'valor_tarifa' => '3835313.48',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'id' => 727,
                'id_cup' => 3677,
                'valor_tarifa' => '3362831.84',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'id' => 728,
                'id_cup' => 3678,
                'valor_tarifa' => '2181130.48',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'id' => 729,
                'id_cup' => 3679,
                'valor_tarifa' => '5046.66',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'id' => 730,
                'id_cup' => 3680,
                'valor_tarifa' => '5046.66',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'id' => 731,
                'id_cup' => 3681,
                'valor_tarifa' => '5046.66',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'id' => 732,
                'id_cup' => 3682,
                'valor_tarifa' => '19276.76',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'id' => 733,
                'id_cup' => 3686,
                'valor_tarifa' => '13754',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'id' => 734,
                'id_cup' => 3688,
                'valor_tarifa' => '5046.66',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'id' => 735,
                'id_cup' => 3694,
                'valor_tarifa' => '2383589.36',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'id' => 736,
                'id_cup' => 3695,
                'valor_tarifa' => '5663844.3',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'id' => 737,
                'id_cup' => 3696,
                'valor_tarifa' => '5373211.7',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'id' => 738,
                'id_cup' => 3697,
                'valor_tarifa' => '4961120.7',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'id' => 739,
                'id_cup' => 3698,
                'valor_tarifa' => '7436692.58',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'id' => 740,
                'id_cup' => 3699,
                'valor_tarifa' => '249899.6',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'id' => 741,
                'id_cup' => 3701,
                'valor_tarifa' => '649178.22',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'id' => 742,
                'id_cup' => 3702,
                'valor_tarifa' => '1746398.28',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'id' => 743,
                'id_cup' => 3705,
                'valor_tarifa' => '894771.76',
                'anno' => '2019',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}