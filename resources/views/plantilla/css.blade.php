<!-- Bootstrap core CSS-->
<link href="{{url('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="{{url('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="{{url('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="{{url('css/sb-admin.css')}}" rel="stylesheet">
<link href="{{url('css/sweetalert2.css')}}" rel="stylesheet">
<style>.row{padding-top: 15px;}.btn-focus:focus {background-color: red;color:#FFF;}.label-danger {background-color: #d9534f}.label-success {background-color: hsl(120, 39%, 54%);}.label {display: inline;padding: .2em .6em .3em;font-size: 75%;font-weight: 700;line-height: 1;color: hsl(0, 0%, 100%);text-align: center;white-space: nowrap;vertical-align: baseline;border-radius: .25em;}.has-error .form-control {border-color: hsl(1, 44%, 46%);-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);box-shadow: inset 0 1px 1px rgba(0,0,0,.075);}
</style>