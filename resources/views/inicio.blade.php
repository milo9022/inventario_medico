<!DOCTYPE html>
<html lang="es">
	<head>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>@yield('title')</title>
		@include('plantilla.css')
		@include('plantilla.js')
		</head>
	<body id="page-top">
    <app></app>
    <script src="{{ mix('js/app.js') }}"></script>
	</body>
</html>