require('./bootstrap')
import Vue from 'vue'
import Notifications from 'vue-notification'
import VueRouter from 'vue-router'
import Swal from 'sweetalert2'

Vue.use(VueRouter)
Vue.use(Notifications)

import App from './views/App'
import Cups from './views/cups/Cups'
import CupsEdit from './views/cups/Cupsedit'
import CupsNew from './views/cups/CupsNew'
import CupsCantidad from './views/cups/Cupscantidad'

import Users from './views/user/Users'
import UsersEdit from './views/user/UsersEdit'
import UsersNew from './views/user/UsersNew'
import password from './views/user/changepass';

import Prestadores from './views/prestadores/Prestadores'
import PrestadoresEdit from './views/prestadores/PrestadoresEdit'
import PrestadoresNew from './views/prestadores/Prestadoresnew'


import Centrocostos from './views/centrocostos/Centrocostos'
import CentrocostosNew from './views/centrocostos/CentrocostosNew'
import ingresos_tipo from './views/centrocostos/ingresos_tipo'


import costosgastos from './views/informes/costosgastos';
import informes_tipo from './views/informes/informes_tipo';
import insumosInformes from './views/informes/informe_insumo';
import informes_cups from './views/informes/informes_cups';

const router = new VueRouter({
    mode: 'history',
    routes: [{
            path: '/dashboard/',
            name: 'cups',
            component: Cups,
        },
        {
            path: '/dashboard',
            name: 'cups',
            component: Cups,
        },
        {
            path: '/dashboard/cups',
            name: 'cups',
            component: Cups,
        },
        {
            path: '/dashboard/cups/editar/:id',
            name: 'cupsedit',
            component: CupsEdit,
        },
        {
            path: '/dashboard/cups/nuevo',
            name: 'cupsnew',
            component: CupsNew,
        },
        {
            path: '/dashboard/cups/cantidad',
            name: 'cupscantidad',
            component: CupsCantidad,
        },
        {
            path: '/dashboard/insumos_informes',
            name: 'insumos',
            component: insumosInformes,
        },

        {
            path: '/dashboard/usuarios',
            name: 'users',
            component: Users,
        },
        {
            path: '/dashboard/usuarios/edit/:id',
            name: 'usersedit',
            component: UsersEdit,
        },
        {
            path: '/dashboard/usuarios/nuevo',
            name: 'usersnew',
            component: UsersNew,
        },

        //PRESTADORES DE SERVICIO
        {
            path: '/dashboard/prestadores',
            name: 'prestadores',
            component: Prestadores,
        },
        {
            path: '/dashboard/prestadores/edit/:id',
            name: 'prestadoresedit',
            component: PrestadoresEdit,
        },
        {
            path: '/dashboard/prestadores/nuevo',
            name: 'prestadoresnew',
            component: PrestadoresNew,
        },
        //PRESTADORES DE SERVICIO

        //COSTOS
        {
            path: '/dashboard/centrocostos',
            name: 'centrocostos',
            component: Centrocostos,
        }, {
            path: '/dashboard/centrocostos/nuevo',
            name: 'centronew',
            component: CentrocostosNew,
        }, {
            path: '/dashboard/centrocostos/relacion',
            name: 'ingresos_tipo',
            component: ingresos_tipo,
        },
        {
            path: '/dashboard/password/cambiar',
            name: 'password',
            component: password,
        },

        //COSTOS
        //INFORMES
        {
            path: '/dashboard/informes/costosgastos',
            name: 'costosgastos',
            component: costosgastos,
        },
        {
            path: '/dashboard/informes/tipo/:nombre',
            name: 'informes_tipo',
            component: informes_tipo,
        },
        {
            path: '/dashboard/informes/cups',
            name: 'informes_cups',
            component: informes_cups,
        },
        //INFORMES
    ],
});

const app = new Vue({
    el: 'app',
    components: { App },
    router,
});