<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Auth;
use Hash;

class UsuariosController extends Controller
{
    public function list()
    {
        return ['validate'=>TRUE,'msj'=>NULL,'data'=>User::all()];
    }
    public function editPass(Request $request)
    {
        $pass=auth()->user()->password;
        $validate   = false;
        $message    = '';
        if($request->pass1===$request->pass2)
        {
            if (Hash::check($request->passold, $pass))
            {
                $validate = true;
                $message='La contraseña fue cambiada con éxito';
                $data = User::find(auth()->user()->id);
                $data->password=bcrypt($request->pass2);
                $data->save();
            }
            else
            {
                $validate=false;
                $message='La contraseña actual no coincide';                
            }
        }
        else
        {
            $validate=false;
            $message='Las contraseñas no coinciden';
        }
        return [
            'validate'=>$validate,'message'=>$message
            ];
    }
    public function new()
    {
        
    }
    public function edit($id)
    {
        $data=User::find($id);
        $data->roles = Role::all();
        $data->rol = 1;
        return $data;
    }
    public function restablecer(Request $request)
    {
        $data           = User::find($request->id);
        $data->password = bcrypt($data->documento);
        $data->save();
        return ['validate'=>true];
    }
    public function roles()
    {
        return Role::all();
    }
    public function newsave(Request $request)
    {
        $user            = new User();
        $user->name      = $request->input('name');
        $user->email     = $request->input('email');
        $user->password  = bcrypt($request->input('documento'));
        $user->documento = $request->input('documento');
        $user->save();
        $roles = Role::where('id', $request->input('rol'))->first();
        $user->roles()->attach($roles);
        return ['validate',true];
    }
    public function editsave($id,Request $request)
    {
        $role_user =  Role::where('name', $request->input('rol'))->first();
        $user =User::find($id);
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->isblocked = $request->input('isblocked');
        $user->documento = $request->input('documento');
        $user->save();
        $user->roles()->attach($role_user);
        return ['validate'=>true];
    }
    
    
}