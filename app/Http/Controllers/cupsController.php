<?php

namespace App\Http\Controllers;
use \App\Model\TblCups;
use \App\Model\TblCupsTarifa;
use \App\Model\TblCupsIngresosTipo;
use \App\Model\TblCupsIngresosTipoXCups;
use \App\Model\TblCentroCostos;
use \App\Model\TblCentroCostostipo;
use \App\Model\TblCupsXPrestadorServicio;
use \App\Model\TblCupsXPrestadorServicioXAnno;
use DB;
use Illuminate\Http\Request;

class cupsController extends Controller
{
    public function allcentrocostos(Request $request)
    {
        $data=array();
        if(trim($request->id_centro_costos)!='')
        {
            if(!is_null($request->input('year')))
            {
                $year = date('Y',strtotime($request->input('year')));
                $data = TblCentroCostosTipo::where('id','=',$request->id_centro_costos_tipo)->get();
                foreach($data as $key => $temp)
                {
                    $centroCostos=TblCentroCostos::where('id_centro_costos_tipo','=',$temp->id);
                    if(!is_null($request->id_centro_costos))
                    {   
                        $centroCostos->where('id','=',$request->id_centro_costos);
                    }
                    $centroCostos=$centroCostos->get();
                    foreach($centroCostos as $key2 => $temp2)
                    {
                        $res=TblCups::
                        select(
                            'tbl_cups.id',
                            'tbl_cups.codigo',
                            'tbl_cups.nombre',
                            DB::raw($request->year.' AS anno')
                        )
                        ->where('tbl_cups.id_centro_costos', '=', $temp2->id)
                        ->get();
                        foreach($res as $key3 => $temp4)
                        {
                            $id_cups_x_prestador_servicio_anno
                            =
                            TblCupsXPrestadorServicioXAnno::firstOrNew(
                                [
                                    'id_prestador_servicio'=>$request->id_prestador_servicio,
                                    'anno'=>$request->year
                                ]
                            );
                            if(is_null($id_cups_x_prestador_servicio_anno->id))
                            {
                                $id_cups_x_prestador_servicio_anno->id_prestador_servicio = $request->id_prestador_servicio;
                                $id_cups_x_prestador_servicio_anno->anno                  = $request->year;
                                $id_cups_x_prestador_servicio_anno->porcentaje            = 0;
                                $id_cups_x_prestador_servicio_anno->save();
                            }
                            $id_cups_x_prestador_servicio_anno=$id_cups_x_prestador_servicio_anno->id;
                            $valor_tarifa =
                            TblCupsXPrestadorServicio::
                            select('tarifa_valor')->
                            where('id_cups','=',$temp4->id)->
                            where('id_cups_x_prestador_servicios_x_anno','=',$id_cups_x_prestador_servicio_anno)->
                            first();
                            $valor_tarifa=(!isset($valor_tarifa->tarifa_valor))?0:$valor_tarifa->tarifa_valor;
                            $valor_tarifa_original =
                            TblCupsTarifa
                            ::select('tbl_cups_tarifa.valor_tarifa')
                            ->where('tbl_cups_tarifa.id_cup','=',$temp4->id)
                            ->where('tbl_cups_tarifa.anno','=',$request->year)
                            ->first();

                            
                            

                            $valor_tarifa_original            = (is_null($valor_tarifa_original))?0:$valor_tarifa_original->valor_tarifa;
                            $res[$key3]->valor_tarifa_original = $valor_tarifa_original;
                            $res[$key3]->valor_tarifa          = $valor_tarifa;
                        }
                        $centroCostos[$key2]->cups=$res;
                    }
                    $data[$key]->centro_costos=$centroCostos; 
                }
            }
        }
        return ($data);
    }   
    public function Buscar($date,$prestadorServicio,$name)
    {
        
        $name=trim(strtoupper($name));
        $data=TblCentroCostos
        ::orderBy('tbl_cups.codigo')
        ->orderBy('tbl_centro_costos_tipo.nombre')
        ->orderBy('tbl_centro_costos.nombre')
        ->select(
            'tbl_cups.*',
            'tbl_cups.id as id_cups',
            DB::raw('CONCAT_WS(
                \' - \',
                tbl_cups.codigo,
                tbl_centro_costos_tipo.nombre,
                tbl_centro_costos.nombre,
                tbl_cups.nombre) as name'),
            'tbl_centro_costos.*',
            'tbl_centro_costos.nombre as costos_nombre',
            'tbl_centro_costos_tipo.nombre as costos_tipo_nombre'
        )
        ->join('tbl_centro_costos_tipo', 'tbl_centro_costos.id_centro_costos_tipo', '=','tbl_centro_costos_tipo.id')
        ->join('tbl_cups', 'tbl_centro_costos.id', '=','tbl_cups.id_centro_costos')
        ->where('tbl_cups.nombre', 'ILIKE', "%{$name}%") 
        ->orWhere('tbl_cups.codigo', 'ILIKE', "%{$name}%") 
        ->orWhere('tbl_centro_costos.nombre', 'ILIKE',"%{$name}%")
        ->orWhere('tbl_centro_costos_tipo.nombre', 'ILIKE',"%{$name}%")
        ->get();
        $year=explode(' ',$date);
        $year=$year[3];
        foreach($data as $key=>$temp)
        {
            $valor = TblCupsXPrestadorServicio
                    ::select('tarifa_valor')
                    ->join('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno','=','tbl_cups_x_prestador_servicio_anno.id')
                    ->where('tbl_cups_x_prestador_servicio_anno.anno' ,'=', $year) 
                    ->where('tbl_cups_x_prestador_servicios.id_cups' ,'=', $temp->id_cups)
                    ->where('public.tbl_cups_x_prestador_servicio_anno.id_prestador_servicio','=',$prestadorServicio)
                    ->first();
            $temp->valor_tarifa = (isset($valor->tarifa_valor)) ? $valor->tarifa_valor : 0;
            $data[$key]=$temp;
        }
        return $data;
    }
    public function CentroCostos($id_tipo)
    {
        return TblCentroCostos::where('id_centro_costos_tipo','=',$id_tipo)->orderBy('codigo_ceco')->get();
    }
    public function CentroCostosTipos()
    {
        return TblCentroCostostipo::orderBy('nombre')->get();
    }
    private function CentroCostosTipo($response)
    {

        $year=$response['year'];
        $data = TblCentroCostostipo::orderBy('nombre');
        if(isset($response['id_centro_costos_tipo']))
        {
            if(!is_null($response['id_centro_costos_tipo']))
            {
                $data = $data->where('id','=',$response['id_centro_costos_tipo']);
            }
        }
        $data=$data->get();
        foreach($data as $key=> $temp)
        {
            $centroCostos=TblCentroCostos::where('id_centro_costos_tipo','=',$temp->id)->orderBy('codigo_ceco');
            if(isset($response['id_centro_costos_tipo']))
            {
                if(!is_null($response['id_centro_costos']))
                {
                    $centroCostos=$centroCostos->where('id','=',$response['id_centro_costos']);
                }
            }
            $resultado=array();
            $centroCostos=$centroCostos->get();
            foreach($centroCostos as $key1 => $temp1)
            {
                $temp1->cups=TblCups::where('id_centro_costos','=',$temp1->id)->get();
                $valores=array();
                foreach($temp1->cups as $key3=>$temp2)
                {
                    $valor1 = TblCupsTarifa::
                        select('valor_tarifa')->
                        where('id_cup','=',$temp2->id)->
                        where('anno','=',$year)->
                        first();
                    $valor2 = TblCupsXPrestadorServicio
                    ::select('tarifa_valor')
                    ->join('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno','=','tbl_cups_x_prestador_servicio_anno.id')
                    ->where('tbl_cups_x_prestador_servicio_anno.anno' ,'=', $year) 
                    ->where('tbl_cups_x_prestador_servicios.id_cups' ,'=', $temp2->id)
                    ->first();
                    $temp1->cups[$key3]->valor_tarifa               = (isset($valor2->tarifa_valor)) ? $valor2->tarifa_valor : 0;
                    $temp1->cups[$key3]->valor_tarifa_original      = (isset($valor1->valor_tarifa)) ? $valor1->valor_tarifa : 0;
                }  
                $resultado[]=$temp1;
            }
            $data[$key]->centro_costos=$resultado;
        }
        return $data;

    }
    public function index(Request $request)
    {
        $data=array();
        if(!is_null($request->input('year')))
        {
            $year = date('Y',strtotime($request->input('year')));
            $data = TblCentroCostosTipo::where('id','=',$request->id_centro_costos_tipo)->get();
            foreach($data as $key => $temp)
            {
                $centroCostos=TblCentroCostos::where('id_centro_costos_tipo','=',$temp->id);
                if(!is_null($request->id_centro_costos))
                {   
                    $centroCostos->where('id','=',$request->id_centro_costos);
                }
                $centroCostos=$centroCostos->get();
                foreach($centroCostos as $key2 => $temp2)
                {
                    $res=TblCupsTarifa::
                        select(
                            'tbl_cups.codigo',
                            'tbl_cups.nombre',
                            'tbl_cups.id',
                            'tbl_cups_tarifa.valor_tarifa',
                            'tbl_cups_tarifa.valor_tarifa AS valor_tarifa_original',
                            'tbl_cups.id_centro_costos',
                            'tbl_cups_tarifa.anno'
                        )
                    ->join('tbl_cups','tbl_cups_tarifa.id_cup','=','tbl_cups.id')
                    ->where('tbl_cups_tarifa.anno', '=', $request->year)
                    ->where('tbl_cups.id_centro_costos', '=', $temp2->id)
                    ->get();
                    $idsArray=array();
                    foreach($res as $ids)
                    {
                        $idsArray[]=$ids->id;
                    }
                    $idsArray=implode(',',$idsArray);
                    $dataNone=TblCups::
                    select(
                        'tbl_cups.codigo',
                        'tbl_cups.nombre',
                        DB::raw('0 AS valor_tarifa'),
                        DB::raw('0 AS valor_tarifa_original'),
                        DB::raw($temp2->id . 'as id_centro_costos'),
                        DB::raw($request->year . ' as anno')
                    );
                    if($idsArray!='')
                    {
                        $dataNone->where(DB::raw('tbl_cups.id NOT IN ('.$idsArray.')'));
                    }
                    $dataNone=$dataNone->get();
                    foreach($dataNone as $temp6)
                    {
                        $res[]=$temp6;
                    }
                    $centroCostos[$key2]->cups=$res;
                }
                $data[$key]->centro_costos=$centroCostos; 
            }
            echo json_encode($data);
        }
    }
    private function ingresos_tipo($TblCupsid)
    {
        $data=TblCupsIngresosTipo::all();
        foreach($data as $key=>$temp)
        {
            $res = 
            TblCupsIngresosTipoXCups::
            where('id_cups','=',$TblCupsid)->
            where('id_cups_ingresos_tipo','=',$temp->id)->
            first();
            $data[$key]=['id'=>$temp->id,'selected'=>(!is_null($res))];
        }
        return $data;
    }
    public function Relaciones()
    {
        return TblCupsIngresosTipo::all();
    }
    public function InformeCostosGastos($fi,$ff)
    {
        $categorias=TblCupsIngresosTipo::all();
        $data=DB::table('tbl_cups_x_prestador_servicios')
        ->select(

            'tbl_cups_x_prestador_servicio_anno.anno',
            DB::raw('SUM(tbl_costos.valor) as tarifa_valor'),
            'tbl_cups_ingresos_tipo.descripcion'
        )
        ->join('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno',  '=', 'tbl_cups_x_prestador_servicio_anno.id')
        ->join('tbl_cups_ingresos_tipo_x_cups',     'tbl_cups_x_prestador_servicios.id_cups',                               '=', 'tbl_cups_ingresos_tipo_x_cups.id_cups')
        ->join('tbl_cups_ingresos_tipo',            'tbl_cups_ingresos_tipo_x_cups.id_cups_ingresos_tipo',                  '=', 'tbl_cups_ingresos_tipo.id')
        ->join('tbl_costos',                        'tbl_cups_x_prestador_servicios.id',                                    '=', 'tbl_costos.id_cups_x_prestador_servicios')
        
        ->orderBy('tbl_cups_x_prestador_servicio_anno.anno')
        ->orderBy('tbl_cups_ingresos_tipo.id')
        ->groupBy('tbl_cups_ingresos_tipo.id')
        //->groupBy('tbl_costos.valor')
        ->groupBy('tbl_cups_x_prestador_servicio_anno.anno')
        ->whereBetween(DB::raw('date(tbl_costos.fecha)'),[$fi,$ff])
        ->get();
        $relaciones=$this->Relaciones();
        $res=[];
        foreach($relaciones as $temp)
        {
            $res[]=$temp->descripcion;
        }
        $res1=[];
        $res3=[];
        foreach($data as $temp)
        {
            $res3[]=$temp->descripcion;
            $res1[$temp->anno][]= (float)$temp->tarifa_valor;
        }
        $res2=[];
        foreach($res1 as $key=>$temp)
        {
            $temp2=array();
            $temp2['name']=(string)$key;
            $temp2['data']=$temp;
            $res2[]=$temp2;
        }
        $dataFinal=['categorias'=>$res3,'series'=>$res2];
        return $dataFinal;
    }
    public function relacionesSave(Request $request)
    {
        if($request->activo)
        {
            $data = TblCupsIngresosTipoXCups::
            where('id_cups_ingresos_tipo','=',$request->id_categoria)->
            where('id_cups','=',$request->id_cup)->
            get();
            if(count($data)===0)
            {
                $data = new TblCupsIngresosTipoXCups();
                $data->id_cups_ingresos_tipo =$request->id_categoria;
                $data->id_cups =$request->id_cup;
                $data->save();
            }
        }
        else{
            try{

                $data = TblCupsIngresosTipoXCups::
                where('id_cups_ingresos_tipo','=',$request->id_categoria)->
                where('id_cups','=',$request->id_cup)->
                first();
                $data = TblCupsIngresosTipoXCups::find($data->id);
                $data->delete();
            }
            catch(\Exception $ex)
            {

            }
        }
        return ['validate'=>true];
    }
    public function InformeCostosGastosDetalle($name)
    {
        $categorias=TblCupsIngresosTipo::all();
        $data=DB::table('tbl_cups_x_prestador_servicios')
        ->select(
            'tbl_cups_x_prestador_servicio_anno.anno',
            'tbl_cups_x_prestador_servicios.tarifa_valor',
            'tbl_cups_ingresos_tipo.descripcion'
        )
        ->join('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno',  '=', 'tbl_cups_x_prestador_servicio_anno.id')
        ->join('tbl_cups_ingresos_tipo_x_cups',     'tbl_cups_x_prestador_servicios.id_cups',                               '=', 'tbl_cups_ingresos_tipo_x_cups.id_cups')
        ->join('tbl_cups_ingresos_tipo',            'tbl_cups_ingresos_tipo_x_cups.id_cups_ingresos_tipo',                  '=', 'tbl_cups_ingresos_tipo.id')
        
        ->groupBy('tbl_cups_ingresos_tipo.id','tbl_cups_x_prestador_servicio_anno.anno')
        ->orderBy('tbl_cups_x_prestador_servicio_anno.anno','tbl_cups_ingresos_tipo.id')
        ->get();
        
        $relaciones=$this->Relaciones();
        $res=[];
        foreach($relaciones as $temp)
        {
            $res[]=$temp->descripcion;
        }
        $res1=[];
        $res3=[];
        foreach($data as $temp)
        {
            $res3[]=$temp->descripcion;
            $res1[$temp->anno][]=$temp->tarifa_valor;
        }
        $res2=[];
        foreach($res1 as $key=>$temp)
        {
            $temp2=array();
            $temp2['name']=(string)$key;
            $temp2['data']=$temp;
            $res2[]=$temp2;
        }
        $res3=['Enero','Febrero','Marzo','Abril'];
        return ['categorias'=>$res3,'series'=>$res2];
    }
    #-----------------------------------------------------------------------------------------#
    private function CentroCostosTipoTodo($response)
    {
        $data = TblCentroCostostipo::orderBy('nombre');
        if(!is_null($response['id_centro_costos_tipo']))
        {
            $data = $data->where('id','=',$response['id_centro_costos_tipo']);
        }
        $data=$data->get();
        foreach($data as $key=> $temp)
        {
            $centroCostos=TblCentroCostos::where('id_centro_costos_tipo','=',$temp->id)->orderBy('codigo_ceco');
            if(!is_null($response['id_centro_costos']))
            {
                $centroCostos=$centroCostos->where('id','=',$response['id_centro_costos']);
            }
            $resultado=array();
            $centroCostos=$centroCostos->get();
            foreach($centroCostos as $key1 => $temp1)
            {
                $temp1->cups=TblCups::where('id_centro_costos','=',$temp1->id)->get();
                $resultado[]=$temp1;
            }
            $data[$key]->centro_costos=$resultado;
        }
        return $data;

    }

    #-----------------------------------------------------------------------------------------#
    public function RelacionesCup(Request $request)
    {
        $data = array();
        if($request->id_centro_costos_tipo!='-1')
        {
            $data = $this->CentroCostosTipoTodo($request->all());
            foreach($data as $key => $temp)
            {
                foreach($temp->centro_costos as $key1 => $temp1)
                {
                    foreach($temp1->cups as $key2 => $temp2)
                    {
                        $temp2->tipos=$this->ingresos_tipo($temp2->id);
                    }
                }
            }
        }
        return $data;        
    }
    public function VerCup($id)
    {
        return TblCups::find($id);
    }
    public function EditarGuardar(Request $request)
    {
        $data = TblCups::find($request->input('id'));
        $data->nombre=$request->input('nombre');
        $data->codigo=$request->input('codigo');
        $data->save();
        return ($data);
    }
    public function NuevoGuardar(Request $request)
    {
        $data = new TblCups();
        $data->nombre=$request->input('nombre');
        $data->codigo=$request->input('codigo');
        $data->id_centro_costos=$request->input('id_centro_costos');
        $data->save();
        return ($data);
    }
    public function GuardarCupAnno(Request $request)
    {
        try
        {    
            $data = ($request->input('data'));
            $data=$data[0];
            $resultados=($data['centro_costos'][0]['cups']);
            foreach($resultados as $key=>$temp)
            {
                $res = TblCupsTarifa::firstOrNew(
                    [
                        'id_cup'=>$temp['id'] ,
                        'anno'=>$request->input('year')
                    ]
                );
                $res->id_cup=$temp['id'];
                $res->valor_tarifa=0;
                if(isset($temp['valor_tarifa']))
                {
                    $res->valor_tarifa=(is_null($temp['valor_tarifa']))?0:$temp['valor_tarifa'];
                }
                $res->anno=$request->input('year');
                $res->save();
            }
            return ['validate'=>true];
        }
        catch(Exception $e)
        {
            return ['validate'=>false];
        }
    }
    public function borrar(Request $request)
    {
        $data = TblCups::find($request->input('id'));
        $data->delete();
        return ['validate'=>true];
    }
    
}
