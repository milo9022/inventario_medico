<?php

namespace App\Http\Controllers;
use App\Model\TblCupsXPrestadorServicioXAnno;
use App\Model\TblCupsXPrestadorServicio;
use App\Model\TblCups;
use App\Model\TblCupsTarifa;
use App\Model\TblCostos;
use App\Model\TblCostosCategoria;
use App\Model\TblCostosCategoriaDetalle;
use App\Model\TblCupsIngresosTipo;
use App\Model\TblPrestadorServicio;
use DB;
use Illuminate\Http\Request;

class TblCupsXPrestadorServicios extends Controller
{
    private function generarArchivo($string,$nombre='')
    {
        $nombre=($nombre=='')?'Informe.csv':$nombre;
        $ds=DIRECTORY_SEPARATOR;
        $nombre_archivo = public_path()."{$ds}archivos{$ds}{$nombre}";
        if($archivo = fopen($nombre_archivo, "w"))
        {
            fwrite($archivo, $string); 
            fclose($archivo);
        }
        header("Content-disposition: attachment; filename={$nombre}");
        header("Content-type: MIME");
        readfile("{$nombre_archivo}");
    }
    private function str_putcsv($lista,$encabezado=array(),$file="") 
    {
        $txt='';
        if(count($encabezado)>0)
        {
            foreach($encabezado as $temp1)
            {
                foreach($temp1 as $key=> $temp)
                {
                    $temp1[$key] = mb_convert_encoding($temp, "Windows-1252", "UTF-8" );
                }
                $txt.=implode(';',$temp1)."\n";
            }
        }
        foreach($lista as $temp)
        {
            $temp->porcentaje=((1-((double)$temp->valor_vendido/($temp->valor_ofertado==0?1:(double)$temp->valor_ofertado)))*100).'%';
            $temp=json_decode(json_encode($temp),true);            
            foreach($temp as $key=>$temp1)
            {
                $temp1=trim($temp1);
                $temp1 = preg_replace("/[\r\n|\n|\r]+/", ' ', $temp1);
                $temp1 = str_replace(";", ' ', $temp1);
                $temp[$key] = mb_convert_encoding($temp1, "Windows-1252", "UTF-8" );
            }
            $txt.=implode(';',$temp)."\n";
        }
        $this->generarArchivo($txt,$file);
    
    }
    private function PrestadorServicios($data)
    {
        $cups           = TblCups::all();
        foreach($cups as $temp)
        {
            $cupxprestser   = TblCupsXPrestadorServicio::firstOrNew(
                [
                    'id_cups'=>$temp->id,
                    'id_cups_x_prestador_servicios_x_anno'=>$data->id
                    ]
                );
                $tarifa                                             = TblCupsTarifa::where('id_cup','=',$temp->id)->where('anno','=',$data->anno)->first();
                $tarifa                                             = (isset($tarifa->valor_tarifa))?$tarifa->valor_tarifa:0;
                $tarifa                                             = ((100-(-1*$data->porcentaje)/100)*($tarifa));
                $cupxprestser->id_cups                              = $temp->id;
                $cupxprestser->tarifa_valor                         = $tarifa;
                $cupxprestser->id_cups_x_prestador_servicios_x_anno = $data->id;
                $cupxprestser->save();
                unset($cupxprestser);
                
        }
    }
    
    public function informeeps(Request $request)
    {
        $data=TblCupsXPrestadorServicio
        ::select
        (
            'tbl_prestador_servicios.nombre as eps',
            'tbl_centro_costos.codigo_ceco',
            'tbl_centro_costos_tipo.nombre as tipo',
            'tbl_centro_costos.nombre as centro',
            //'tbl_cups.nombre',
            DB::raw('COALESCE(SUM(public.tbl_costos.valor*
            (COALESCE((SELECT public.tbl_cups_cantidad.cantidad_cups 
            FROM 
            public.tbl_cups_cantidad 
            WHERE 
            public.tbl_cups_cantidad.id_cups = public.tbl_cups_x_prestador_servicios.id_cups AND 
            public.tbl_cups_cantidad.id_prestador_servicios = public.tbl_cups_x_prestador_servicio_anno.id_prestador_servicio AND 
            public.tbl_cups_cantidad.fecha = public.tbl_costos.fecha),1))),0) AS valor_vendido'),
            'tbl_cups_x_prestador_servicios.tarifa_valor as valor_ofertado',
            DB::raw('(tbl_cups_x_prestador_servicios.tarifa_valor)-COALESCE(SUM(public.tbl_costos.valor*
            (COALESCE((SELECT public.tbl_cups_cantidad.cantidad_cups 
            FROM 
            public.tbl_cups_cantidad 
            WHERE 
            public.tbl_cups_cantidad.id_cups = public.tbl_cups_x_prestador_servicios.id_cups AND 
            public.tbl_cups_cantidad.id_prestador_servicios = public.tbl_cups_x_prestador_servicio_anno.id_prestador_servicio AND 
            public.tbl_cups_cantidad.fecha = public.tbl_costos.fecha),1))),0) AS valor_diferencia')
            //,'tbl_cups.codigo'
        )        
        ->leftJoin('tbl_cups','tbl_cups_x_prestador_servicios.id_cups','=','tbl_cups.id')
        ->leftJoin('tbl_costos','tbl_cups_x_prestador_servicios.id','=','tbl_costos.id_cups_x_prestador_servicios')
        ->leftJoin('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno','=','tbl_cups_x_prestador_servicio_anno.id')
        ->join('tbl_centro_costos','tbl_cups.id_centro_costos','=','tbl_centro_costos.id')
        ->join('tbl_centro_costos_tipo','tbl_centro_costos.id_centro_costos_tipo','=','tbl_centro_costos_tipo.id')
        ->leftJoin('tbl_prestador_servicios','tbl_cups_x_prestador_servicio_anno.id_prestador_servicio','=', 'tbl_prestador_servicios.id')
        ->where('tbl_cups_x_prestador_servicio_anno.anno','=',$request->year)
        //->groupBy('tbl_cups.codigo')
        //->groupBy('tbl_cups.nombre')
        ->groupBy('tbl_prestador_servicios.nombre')
        ->groupBy('tbl_centro_costos_tipo.nombre')
        ->groupBy('tbl_centro_costos.nombre')
        ->groupBy('tbl_centro_costos.codigo_ceco')
        ->groupBy('tbl_cups_x_prestador_servicios.tarifa_valor')
        ->groupBy('tbl_cups_x_prestador_servicio_anno.id_prestador_servicio')
        ->groupBy('tbl_cups_x_prestador_servicio_anno.anno')
        ->orderBy('tbl_centro_costos.codigo_ceco')
        ->orderBy('tbl_centro_costos_tipo.nombre')
        ->orderBy('tbl_centro_costos.nombre')
        //->orderBy('tbl_cups.codigo')
        ->get();
        $encabezado=[
            ['Año',$request->year],
            ['EPS','Código','unidad de negocio','Centro costos','Total cobrado','Costo procedimiento','Diferencia']
        ];
        $this->str_putcsv($data,$encabezado,'Informes Total.csv');
    }
    private function sumarCostos (Request $request){
        $request->merge(["year"=>$request->fecha.'T21:41:00.000Z']);
        $request->merge(["fecha"=>$request->fecha.'T21:41:00.000Z']);
        $request->merge(["prestador"=>$request->prestadorServicio]);
        $res = $this->listar($request);
        $total=0;
        foreach($res as $temp)
        {
            foreach($temp->detalle as $temp2)
            {
                $total=$total+($temp2->valor==''?0:$temp2->valor);
            }
        }
        return $total;
    }
    public function InformesCupsXPrestador(Request $request)
    {
        $fecha=$request->fecha;
        $id_eps=$request->eps;
        $epsNombre=TblPrestadorServicio::find($id_eps);
        $epsNombre=$epsNombre->nombre;
        $year=date('Y',strtotime($fecha));
        $request->merge(["prestadorServicio"=>$id_eps]);
        $request->merge(["year"=>$year]);
        $porcentaje=$this->data($request);
        $porcentaje=($porcentaje['porcentaje']==''?0:$porcentaje['porcentaje']);
        $data = 
        TblCupsXPrestadorServicio::select(
            'tbl_cups_x_prestador_servicios.id_cups as id',
            'tbl_cups.codigo',
            'tbl_cups.nombre',
            DB::raw('\''.($porcentaje>0?'+'.$porcentaje:$porcentaje).'\' as porcentajedescuentoeps'),
            DB::raw('\'\' as preciocuptotal'),
            DB::raw('\'\' as preciocupcondescuetoeps'),
            DB::raw('\'\' as costocup'),
            DB::raw('\'\' as diferencia'),
            DB::raw('\'\' as porcentajedeganacia')
        )->
        join('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno', '=', 'tbl_cups_x_prestador_servicio_anno.id')->
        join('tbl_cups','tbl_cups_x_prestador_servicios.id_cups', '=', 'tbl_cups.id')->
        where('tbl_cups_x_prestador_servicio_anno.anno',                 '=', $year)->
        where('tbl_cups_x_prestador_servicio_anno.id_prestador_servicio','=', $id_eps)->
        get();
        foreach($data as $key=>$temp)
        {
            $preciocupcondescuetoeps=0;
            $costocup=0;
            $preciocupcondescuetoepsid=            
            TblCupsXPrestadorServicioXAnno::
            where('id_prestador_servicio','=',$id_eps)->
            where('anno','=',$year)->
            first();
            if(!is_null($preciocupcondescuetoepsid->id))
            {
                $id_cups_x_prestador_servicio_anno=$preciocupcondescuetoepsid->id;
                $valor_tarifa =
                TblCupsXPrestadorServicio::
                select('tarifa_valor')->
                where('id_cups','=',$temp->id)->
                where('id_cups_x_prestador_servicios_x_anno','=',$id_cups_x_prestador_servicio_anno)->
                first();
                $preciocupcondescuetoeps=(!isset($valor_tarifa->tarifa_valor))?0:$valor_tarifa->tarifa_valor;
            }
            $preciocuptotal=
                TblCupsTarifa::
                where('id_cup','=',$temp->id)->
                where('anno','=',$year)->
                first();
            $request->merge(["id_cup"=>$temp->id]);
            $costocup = $this->sumarCostos($request);
            $data[$key]->preciocupcondescuetoeps    = $preciocupcondescuetoeps;
            $data[$key]->preciocuptotal             = (is_null($preciocuptotal)?0:$preciocuptotal->valor_tarifa);
            $data[$key]->costocup                   = $costocup;
            $data[$key]->diferencia                 = $preciocupcondescuetoeps-$costocup;
            $data[$key]->porcentajedeganacia        = ((1-($preciocupcondescuetoeps==0?0:$costocup/$preciocupcondescuetoeps))*100).'%';
            unset($data[$key]->id);
        }
        $encabezado=[
            ['EPS',$epsNombre],
            ['Año',$year],
            ['codigo cup','decripcion cup','porcentaje descuento a eps','precion cup total','precio cup con descueto eps','costo cup','diferencia','porcentaje de ganacia']
        ];
        $this->str_putcsv($data,$encabezado);
    }
    public function informeInsumos(Request $request)
    {
        $Fechas=array();
        $fechaInicio= $request->fechaInicio;
        $fechaFin   = $request->fechaFin;
        $data=array();

        $date1 = new \DateTime($fechaInicio);
        $date2 = new \DateTime($fechaFin);
        $diff = $date1->diff($date2);
        $total = $diff->days;
        for($i=0;$i<=$total;$i++)
        {
            $fecha=date('Y-m-d',strtotime ( '+'.$i.' day' , strtotime( $fechaInicio )));
            $Fechas[]=$fecha;
            $temp2=array();
            $temp = TblCostos::
            select
            (
                DB::raw('SUM(public.tbl_costos.valor*
                (COALESCE((SELECT public.tbl_cups_cantidad.cantidad_cups 
                FROM 
                public.tbl_cups_cantidad 
                WHERE 
                public.tbl_cups_cantidad.id_cups = public.tbl_cups_x_prestador_servicios.id_cups AND 
                public.tbl_cups_cantidad.id_prestador_servicios = public.tbl_cups_x_prestador_servicio_anno.id_prestador_servicio AND 
                public.tbl_cups_cantidad.fecha = public.tbl_costos.fecha),1))) AS valor'),
                'tbl_costos_categoria_detallle.descripcion',
                'tbl_costos.fecha'
            )
            ->join('tbl_costos_categoria_detallle','tbl_costos.id_costos_categoria_detallle','=','tbl_costos_categoria_detallle.id')
            ->join('tbl_cups_x_prestador_servicios',    'tbl_costos.id_cups_x_prestador_servicios', '=' , 'tbl_cups_x_prestador_servicios.id')
            ->join('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno', '=' , 'tbl_cups_x_prestador_servicio_anno.id')
            ->where('tbl_costos_categoria_detallle.id_costos_categoria','=', '2')
            ->where('tbl_costos.fecha','=',$fecha)
            ->groupBy('tbl_costos_categoria_detallle.descripcion')
            ->groupBy('tbl_costos.fecha')
            ->orderBy('tbl_costos_categoria_detallle.descripcion')
            ->get();
            if(count($temp)==0)
            {
                $temp=TblCostosCategoriaDetalle::where('tbl_costos_categoria_detallle.id_costos_categoria','=', '2')
                ->select(
                    DB::raw('0 as valor'),
                    'tbl_costos_categoria_detallle.descripcion',
                    DB::raw('\''.$fecha.'\' as fecha ')
                    )
                ->orderBy('tbl_costos_categoria_detallle.descripcion')->get();
            }
            foreach($temp as $key=>$temp1)
            {
                $temp5 = array();
                $id    = array_search($temp1->descripcion, array_column($data, 'name'));
                if ($id!==FALSE) 
                {
                    $data[$id]['name']=$temp1->descripcion;
                    $data[$id]['data'][]=(double)$temp1->valor;
                }
                else
                {
                    $tempData=array();
                    $tempData['name']=$temp1->descripcion;
                    $tempData['data'][]=(double)$temp1->valor;
                    $data[]=$tempData;
                }
            }
        }
        return ['validate'=>true,'data'=>['Fechas'=>$Fechas,'value'=>$data]];
    }
    public function informe(Request $request)
    {
        $data=TblCupsXPrestadorServicio
        ::select
        (
            'tbl_centro_costos.codigo_ceco',
            'tbl_cups.codigo',
            'tbl_centro_costos_tipo.nombre as tipo',
            'tbl_centro_costos.nombre as centro',
            'tbl_cups.nombre',
            DB::raw('  COALESCE(SUM(public.tbl_costos.valor*
            (COALESCE((SELECT public.tbl_cups_cantidad.cantidad_cups 
            FROM 
            public.tbl_cups_cantidad 
            WHERE 
            public.tbl_cups_cantidad.id_cups = public.tbl_cups_x_prestador_servicios.id_cups AND 
            public.tbl_cups_cantidad.id_prestador_servicios = public.tbl_cups_x_prestador_servicio_anno.id_prestador_servicio AND 
            public.tbl_cups_cantidad.fecha = public.tbl_costos.fecha),1))),0)  AS valor_vendido'),
            'tbl_cups_x_prestador_servicios.tarifa_valor as valor_ofertado',
            DB::raw('(tbl_cups_x_prestador_servicios.tarifa_valor)-COALESCE(SUM(public.tbl_costos.valor*
            (COALESCE((SELECT public.tbl_cups_cantidad.cantidad_cups 
            FROM 
            public.tbl_cups_cantidad 
            WHERE 
            public.tbl_cups_cantidad.id_cups = public.tbl_cups_x_prestador_servicios.id_cups AND 
            public.tbl_cups_cantidad.id_prestador_servicios = public.tbl_cups_x_prestador_servicio_anno.id_prestador_servicio AND 
            public.tbl_cups_cantidad.fecha = public.tbl_costos.fecha),1))),0) AS valor_diferencia')
        )        
        ->leftJoin('tbl_cups','tbl_cups_x_prestador_servicios.id_cups','=','tbl_cups.id')
        ->leftJoin('tbl_costos','tbl_cups_x_prestador_servicios.id','=','tbl_costos.id_cups_x_prestador_servicios')
        ->leftJoin('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno','=','tbl_cups_x_prestador_servicio_anno.id')
        ->join('tbl_centro_costos','tbl_cups.id_centro_costos','=','tbl_centro_costos.id')
        ->join('tbl_centro_costos_tipo','tbl_centro_costos.id_centro_costos_tipo','=','tbl_centro_costos_tipo.id')
        ->where('tbl_cups_x_prestador_servicio_anno.id_prestador_servicio','=',$request->id_eps)
        ->where('tbl_cups_x_prestador_servicio_anno.anno','=',$request->year)
        ->groupBy('tbl_cups.codigo')
        ->groupBy('tbl_cups.nombre')
        ->groupBy('tbl_centro_costos.codigo_ceco')
        ->groupBy('tbl_centro_costos_tipo.nombre')
        ->groupBy('tbl_centro_costos.nombre')
        ->groupBy('tbl_centro_costos.codigo_ceco')
        ->groupBy('tbl_cups_x_prestador_servicios.tarifa_valor')
        ->groupBy('tbl_cups_x_prestador_servicio_anno.id_prestador_servicio')
        ->groupBy('tbl_cups_x_prestador_servicio_anno.anno')
        ->orderBy('tbl_centro_costos.codigo_ceco')
        ->orderBy('tbl_cups.codigo')
        ->get();
        $eps=TblPrestadorServicio::find($request->id_eps);
        $encabezado=[
            ['EPS',$eps->nombre],
            ['Año',$request->year],
            ['Código CECO','Codigo CUP','unidad de negocio','Centro costos','CUP','Total cobrado','Costo procedimiento','Diferencia','% diferencia']
        ];
        $this->str_putcsv($data,$encabezado);
    }
    
    public function save(Request $request)
    {
        if(!is_null($request->input('porcentaje')))
        {
            $datares=array();
            $cups=$request->cups;
            $data =TblCupsXPrestadorServicioXAnno::firstOrNew(
                [
                    'id_prestador_servicio'=>$request->input('prestadorServicio'),
                    'anno'=>$request->input('year')
                ]
            );
            $data->id_prestador_servicio=$request->input('prestadorServicio');
            $data->anno=$request->input('year');
            $data->porcentaje=$request->input('porcentaje');
            $data->save();
            foreach($cups as $temp)
            {
                foreach($temp['centro_costos'] as $temp1)
                {
                    foreach($temp1['cups'] as $temp2)
                    {
                        $tempdata=array();
                        $temp2=(object)$temp2;
                        $res=TblCupsXPrestadorServicio::firstOrNew(
                            [
                                'id_cups'=>$temp2->id,
                                'id_cups_x_prestador_servicios_x_anno'=>$data->id
                            ]
                        );
                        $res->id_cups=$temp2->id;
                        $res->tarifa_valor=$temp2->valor_tarifa;
                        $res->id_cups_x_prestador_servicios_x_anno=$data->id;
                        $res->save();
                    }   
                }
            }
            return ['validate'=>true,'id'=>$data->id];
        }
        else{
            return ['validate'=>false,'id'=>null, 'data'=>null];

        }
    }
    public function data(Request $request)
    {
        $porcentaje = TblCupsXPrestadorServicioXAnno
        ::where('id_prestador_servicio','=',$request->input('prestadorServicio'))
        ->where('anno','=',$request->input('year'))
        ->first();
        return isset($porcentaje->porcentaje)?(['porcentaje'=>$porcentaje->porcentaje]):['porcentaje'=>''];
    }
    private function ValorCostoCategoriaDetalle($id_prestador_servicio,$year,$id_cup,$id_costos_categoria_detallle,$fecha)
    {
        $fecha=explode('T',$fecha);
        $fecha=$fecha[0];
        $data = 
        TblCupsXPrestadorServicio::
        select(
            'tbl_costos.valor'
        )
        ->where('tbl_costos.fecha','=',$fecha)
        ->where('tbl_cups.id','=', $id_cup)
        ->where('tbl_cups_x_prestador_servicio_anno.id_prestador_servicio','=',$id_prestador_servicio)     
        ->where('tbl_costos.id_costos_categoria_detallle','=',$id_costos_categoria_detallle)
        ->join('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno', '=', 'tbl_cups_x_prestador_servicio_anno.id')
        ->join('tbl_costos','tbl_cups_x_prestador_servicios.id', '=', 'tbl_costos.id_cups_x_prestador_servicios')
        ->join('tbl_prestador_servicios','tbl_cups_x_prestador_servicio_anno.id_prestador_servicio', '=', 'tbl_prestador_servicios.id')
        ->join('tbl_cups','tbl_cups_x_prestador_servicios.id_cups', '=', 'tbl_cups.id')
        ->first();
        return (isset($data->valor))?$data->valor:'';
    }
    private function fecha($fecha)
    {
        return $fecha;
        $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ('Y-m-d', $nuevafecha);
        return $nuevafecha;
    }
    public function listar(Request $request)
    {
        
        $data=array();
        $TblCostosCategoria = TblCostosCategoria::all();
        $id_prestador_servicio = $request->input('prestador');
        $fecha                 = $this->fecha($request->input('fecha'));
        $year                  = $request->input('year');
        $year=explode('-',$year);
        $year=$year[0];
        $id_cup                = $request->input('id_cup');
        $datares=[];
        if(!is_null($year)&&!is_null($id_cup))
        {
            foreach($TblCostosCategoria as $key=>$temp)
            {
                $temp->detalle=TblCostosCategoriaDetalle::where('id_costos_categoria','=',$temp->id)->get();
                foreach($temp->detalle as $key=>$temp3)
                {
                    $temp->detalle[$key]->valor =  $this->ValorCostoCategoriaDetalle($id_prestador_servicio,$year,$id_cup,$temp3->id,$fecha);
                }
                $data[]=$temp;
            }
        }
        return $data;
    }
    private function saveCosto($id_costos_categoria_detallle,$id_cup,$id_prestador_servicios,$Fecha,$valor)
    {        
        $year=date('Y',strtotime($Fecha));
        $temp1=TblCupsXPrestadorServicioXAnno::
        where('id_prestador_servicio','=',$id_prestador_servicios)
        ->where('anno','=',$year)->first();
        if(!is_null($temp1->id))
        {
            $temp2=TblCupsXPrestadorServicio::firstOrNew(
                [
                    'id_cups_x_prestador_servicios_x_anno'=>$temp1->id,
                    'id_cups'=>$id_cup
                ]
            );
            $temp2->tarifa_valor=is_null($temp2->tarifa_valor)?0:$temp2->tarifa_valor;
            $temp2->save();
            $id_cups_x_prestador_servicios=$temp2->id;
            $data = TblCostos::firstOrNew(
                [
                    'id_cups_x_prestador_servicios'=>$id_cups_x_prestador_servicios,
                    'id_costos_categoria_detallle'=>$id_costos_categoria_detallle,
                    'fecha'=>$Fecha
                ]
            );
        }
        else
        {           
            $prestador = new TblCupsXPrestadorServicioXAnno();
            $prestador->id_prestador_servicio=$id_prestador_servicios;
            $prestador->anno=$year;
            $prestador->porcentaje=0;
            $prestador->save();
            
            $new                                        = new TblCupsXPrestadorServicio();
            $new->id_cups                               = $id_cup;
            $new->tarifa_valor                          = 0;
            $new->id_cups_x_prestador_servicios_x_anno  = $prestador->id;
            $new->save();
            $data = new TblCostos();
            $id_cups_x_prestador_servicios=$new->id;
        }
        $data->id_cups_x_prestador_servicios = $id_cups_x_prestador_servicios;
        $data->id_costos_categoria_detallle  = $id_costos_categoria_detallle;
        $data->valor                         = is_null($valor)?0:$valor;
        $data->fecha                         = $Fecha;
        $data->save();
    }
    private function EPSs($id_prestador_actual)
    {
        $data = TblCupsXPrestadorServicioXAnno::
        select('tbl_prestador_servicios.id')->
        join('tbl_prestador_servicios','tbl_cups_x_prestador_servicio_anno.id_prestador_servicio','=','tbl_prestador_servicios.id')->
        groupBy('tbl_prestador_servicios.id')->
        where('tbl_prestador_servicios.id','!=',$id_prestador_actual)->
        get();
        return $data; 
    }
    public function saveCostos(Request $request)
    {
        $arrayCategorias = $request->input('arrayCategorias');
        $cup = $request->input('cup');
        $prestadorServicio = $request->input('prestadorServicio');
        $year = $this->fecha($request->input('year'));
        $epss = $this->EPSs($prestadorServicio);
        $this->guardarOtros($epss,$arrayCategorias,$cup,$prestadorServicio,$year);
        foreach($arrayCategorias as $temp)
        {
            foreach($temp['detalle'] as $temp2)
            {
                $temp2['valor']=is_null($temp2['valor'])?0:$temp2['valor'];
                $this->saveCosto(($temp2['id']),$cup,$prestadorServicio,$year,$temp2['valor']);
                
            }
        }
        
        return ['validate'=>true];
    }
    private function guardarOtros($epss,$arrayCategorias,$cup,$prestadorServicio,$year)
    {
        $dataEps=array();
        $resEps=array();
        foreach($epss as $eps)
        {
            $validate=true;
            $request = new Request();
            $request->merge([
                'prestador' =>$eps->id,
                'fecha'     =>$year,
                'year'      =>$year,
                'id_cup'    =>$cup
            ]); 
            $data=$this->listar($request);
            foreach($data as $temp)
            {
                foreach($temp->detalle as $temp1)
                {
                    foreach($temp1 as $temp2)
                    {
                        if(trim($temp1->valor)!='' ||$temp1->valor=='0' )
                        {
                            $validate=false;
                        }
                    }
                }
            }
            if($validate)
            {
                $dataEps[]=$eps->id;
            }
        }
        foreach($dataEps as $eps)
        {
            foreach($arrayCategorias as $temp)
            {
                foreach($temp['detalle'] as $temp2)
                {
                    $temp2['valor']=is_null($temp2['valor'])?0:$temp2['valor'];
                    $this->saveCosto(($temp2['id']),$cup,$eps,$year,$temp2['valor']);
                }
            }
        }
    }
}