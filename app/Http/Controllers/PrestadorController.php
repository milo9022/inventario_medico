<?php

namespace App\Http\Controllers;

use App\Model\TblPrestadorServicio;
use App\Model\TblCupsXPrestadorServicioXAnno;
use Illuminate\Http\Request;

class PrestadorController extends Controller
{
    public function list()
    {
        $data = TblPrestadorServicio::orderBy('nombre')->get();
        foreach($data as $key=>$temp)
        {
            $data[$key]->annos=TblCupsXPrestadorServicioXAnno::select('anno')->where('id_prestador_servicio','=',$temp->id)->get();
        }
        return $data; 
    }
    public function listRegister()
    {
        $data = TblCupsXPrestadorServicioXAnno::
        select('tbl_prestador_servicios.*')->
        join('tbl_prestador_servicios','tbl_cups_x_prestador_servicio_anno.id_prestador_servicio','=','tbl_prestador_servicios.id')->
        groupBy('tbl_prestador_servicios.id')->
        get();
        foreach($data as $key=>$temp)
        {
            $data[$key]->annos=TblCupsXPrestadorServicioXAnno::select('anno')->where('id_prestador_servicio','=',$temp->id)->get();
        }
        return $data; 
    }
    public function edit($id)
    {
        return TblPrestadorServicio::find($id);
    }
    public function editsave($id,Request $request)
    {
        $data = TblPrestadorServicio::find($id);
        $data->nombre       = $request->input('nombre');
        $data->descripcion  = $request->input('descripcion');
        $data->save();
    }
    public function newsave(Request $request)
    {
        $data = new TblPrestadorServicio();
        $data->nombre       = $request->input('nombre');
        $data->descripcion  = $request->input('descripcion');
        $data->save();
        return ['validate'=>true];
    }
    public function borrar(Request $request)
    {
        $data = TblPrestadorServicio::find($request->input('id'));
        $data->delete();
        return ['validate'=>true];
    }
}
