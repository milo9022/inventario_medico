<?php

namespace App\Http\Controllers;
use App\Model\TblCupsCantidad;
use App\Model\TblCupsXPrestadorServicio;
use Illuminate\Http\Request;
use DB;

class TblCupsCantidadController extends Controller
{
    public function save(Request $request)
    {
        $data = TblCupsCantidad::firstOrNew(
            [
                'id_cups'                => $request->id_cups,
                'id_prestador_servicios' => $request->id_prestador_servicios,
                'fecha'                  => $request->fecha
            ]
        );
        $data->id_cups                = $request->id_cups;
        $data->id_prestador_servicios = $request->id_prestador_servicios;
        $data->fecha                  = $request->fecha;
        $data->cantidad_cups          = $request->cantidad_cups;
        $data->save();
        return ['validate'=>true];
    }
    public function index(Request $request)
    {
        $data = TblCupsCantidad
                ::where('id_cups','=',$request->id_cups)
                ->where('id_prestador_servicios','=',$request->id_prestador_servicios)
                ->get();
        foreach($data as $key=>$temp)
        {
            $res=TblCupsXPrestadorServicio::
            select(DB::raw('SUM(tbl_costos.valor) as valor'))
            ->join('tbl_costos','tbl_cups_x_prestador_servicios.id','=','tbl_costos.id_cups_x_prestador_servicios')
            ->join('tbl_cups_x_prestador_servicio_anno','tbl_cups_x_prestador_servicios.id_cups_x_prestador_servicios_x_anno','=','tbl_cups_x_prestador_servicio_anno.id')
            ->where('public.tbl_costos.fecha','=',$temp->fecha)
            ->where('tbl_cups_x_prestador_servicio_anno.id_prestador_servicio','=',$temp->id_prestador_servicios)
            ->where('tbl_cups_x_prestador_servicios.id_cups','=',$temp->id_cups)
            ->first();
            $valor=(is_null($res))?0:($temp->cantidad_cups*$res->valor);
            $valor= number_format($valor,2,'.',',');
            $data[$key]->valor=$valor;
        }
        return ['validate'=>true,'data'=>$data];
    }
}
