<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class TblCostosCategoriaDetalle extends Model
{
    
    use SoftDeletes;
    protected $table = 'tbl_costos_categoria_detallle';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'updated_at',
        'deleted_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'codigo',
        'descripcion',
        'id_costos_categoria'

	];
}
