<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class TblCups extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_cups';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'deleted_at',
        'updated_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'codigo',
        'nombre',
        'id_centro_costos'
    ];
}
