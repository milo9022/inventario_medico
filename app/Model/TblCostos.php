<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class TblCostos extends Model
{    
    use SoftDeletes;
    protected $table = 'tbl_costos';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'updated_at',
        'deleted_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'id_cups_x_prestador_servicios',
        'id_costos_categoria_detallle',
        'valor',
        'fecha'
    ];
}
    