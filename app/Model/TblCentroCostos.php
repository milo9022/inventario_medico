<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TblCentroCostos extends Model
{
    protected $table = 'tbl_centro_costos';
    protected $hidden =[
        'updated_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'nombre',
        'id_centro_costos_tipo',
        'codigo_ceco'
    ];
}
