<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class TblCupsXPrestadorServicio extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_cups_x_prestador_servicios';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'updated_at',
        'deleted_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'id_cups',
        'tarifa_valor',
        'id_cups_x_prestador_servicios_x_anno'
	];
}
