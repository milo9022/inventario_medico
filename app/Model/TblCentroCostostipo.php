<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TblCentroCostostipo extends Model
{
    protected $table = 'tbl_centro_costos_tipo';
    protected $hidden =[
        'updated_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'nombre'
    ];
}
