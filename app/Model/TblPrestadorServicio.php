<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblPrestadorServicio extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_prestador_servicios';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'updated_at',
        'deleted_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'nombre',
        'descripcion'
	];
}
