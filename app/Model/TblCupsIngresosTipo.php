<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblCupsIngresosTipo extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_cups_ingresos_tipo';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'deleted_at',
        'updated_at',
        'created_at'
    ];
    protected $fillable = [
        'descripcion'
	];
}
