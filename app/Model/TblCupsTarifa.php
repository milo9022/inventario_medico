<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblCupsTarifa extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_cups_tarifa';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'updated_at',
        'deleted_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'id_cup',
        'valor_tarifa',
        'anno'

	];
}
