<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblCupsXPrestadorServicioXAnno extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_cups_x_prestador_servicio_anno';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'updated_at',
        'deleted_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'id_prestador_servicio',
        'anno',
        'porcentaje'
	];
}
