<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TblCupsIngresosTipoXCups extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_cups_ingresos_tipo_x_cups';
    protected $dates = ['deleted_at'];
    protected $hidden =[
        'deleted_at',
        'updated_at',
        'created_at'
    ];
    protected $fillable = [
        'id_cups_ingresos_tipo',
        'id_cups'
	];
}
