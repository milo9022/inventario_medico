<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class TblCupsCantidad extends Model
{
    use SoftDeletes;
    protected $table = 'tbl_cups_cantidad';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'deleted_at',
        'updated_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'id_cups',
        'id_prestador_servicios',
        'fecha',
        'cantidad_cups'
    ];
}
