<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class TblCostosCategoria extends Model
{
    
    use SoftDeletes;
    protected $table = 'tbl_costos_categoria';
    protected $dates = ['deleted_at']; 
    protected $hidden =[
        'updated_at',
        'deleted_at',
        'created_at'
    ];
    protected $fillable = [
        'id',
        'descripcion',

	];
}
