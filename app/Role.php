<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Role extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at']; 
    protected $hidden = [
        'updated_at','created_at','deleted_at'
    ];
    public function users()
    {
        return $this
            ->belongsToMany('App\User')
            ->withTimestamps();
    }
}
