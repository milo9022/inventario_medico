<?php
Auth::routes();
Route::get('pass/{pass}',function($pass){return bcrypt($pass);});
Route::post('pass/change','UsuariosController@editPass');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/',function()
{
    return redirect('dashboard');
});
Route::group(['middleware'=>['auth']],function()
{
    Route::prefix('/centrocostos')->group(function () 
    {
        Route::get('select/{id_tipo}', 'cupsController@CentroCostos');
        Route::get('all','cupsController@CentroCostosTipos');
    });
    Route::prefix('/dashboard')->group(function () 
    {
        Route::get('/{any}', 'SpaController@index')->where('any', '.*');
        Route::get('',       'SpaController@index')->where('any', '.*');
    });
    Route::prefix('cups')->group(function () 
    {
        Route::post('saveCantidad',                    'TblCupsCantidadController@save');
        Route::post('Cantidad',                         'TblCupsCantidadController@index');
        Route::get('Search/{date}/{prestadorServicio}/{name}','cupsController@Buscar');
        Route::get('InformeCostosGastosDetalle/{nombre}','cupsController@InformeCostosGastosDetalle');
        Route::post('relacionesSave',                    'cupsController@relacionesSave');
        Route::get('relaciones','cupsController@Relaciones');
        Route::get('InformeCostosGastos/{fi}/{ff}','cupsController@InformeCostosGastos');
        Route::post('cups_relaciones','cupsController@RelacionesCup');
        Route::post('all',      'cupsController@index');
        Route::post('allcentrocostos',      'cupsController@allcentrocostos');
        Route::get('value/{id}','cupsController@VerCup');
        Route::post('editSave', 'cupsController@EditarGuardar');
        Route::post('newSave',  'cupsController@NuevoGuardar');  
        Route::post('saveYear', 'cupsController@GuardarCupAnno');
        Route::post('delete',   'cupsController@borrar');
    });
    Route::prefix('usuarios')->group(function ()
    {
        Route::get('list',          'UsuariosController@list');
        Route::get('roles',         'UsuariosController@roles');
        Route::get('new',           'UsuariosController@new');
        Route::get('edit/{id}',     'UsuariosController@edit');
        Route::post('newsave',      'UsuariosController@newsave');
        Route::post('editsave/{id}','UsuariosController@editsave');   
        Route::post('restablecer',  'UsuariosController@restablecer');
    });
    Route::prefix('prestadores')->group(function () 
    {
        Route::get('list',          'PrestadorController@list');
        Route::get('listRegister',  'PrestadorController@listRegister');
        Route::get('edit/{id}',     'PrestadorController@edit');
        Route::post('newsave',      'PrestadorController@newsave');
        Route::post('editsave/{id}','PrestadorController@editsave');   
        Route::post('delete',       'PrestadorController@borrar');   
    });
    Route::prefix('costos')->group(function () 
    {
        Route::post('listar',               'TblCupsXPrestadorServicios@listar');
        Route::post('save',                 'TblCupsXPrestadorServicios@save');
        Route::post('data',                 'TblCupsXPrestadorServicios@data');
        Route::post('saveCostos',           'TblCupsXPrestadorServicios@saveCostos');
        Route::get('informes',              'TblCupsXPrestadorServicios@informe');
        Route::get('informesEps',           'TblCupsXPrestadorServicios@informeeps');
        Route::get('informesInsumos',       'TblCupsXPrestadorServicios@informeInsumos');
        Route::get('informesInsumos',       'TblCupsXPrestadorServicios@informeInsumos');
        Route::get('InformesCupsXPrestador','TblCupsXPrestadorServicios@InformesCupsXPrestador');
    });
});